#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" The root controller for managing the robotic assembly system.

This controller manages the authority and pattern properties used in
the robotic assembly process. It provides services to set these properties
and publishes them to relevant topics for other components of the system
to use."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import rospy
from std_msgs.msg import String
from controller.srv import Set
from controller.msg import Property
import yaml
import rospkg

class Controller:
    """Controller class for managing properties in the robotic assembly.

    This class publishes the current authority and pattern to specified
    ROS topics and allows these properties to be modified via a service.

    Attributes:
        pattern (str): The current assembly pattern. Default is 'multicolor_square'.
        auth (str): The current authority for the controller. Default is 'cobotic'.
    """
    def __init__(self):
        """Initializes the Controller and sets up publishers and service.

        This method initializes the controller's attributes and creates
        the necessary publishers and service to manage the authority and
        pattern properties.
        """
        # Initialize properties and publishers using dictionaries
        self.properties = {
            'authority': 'cobotic',
            'pattern': 'multicolor_square'
        }

        self.publishers = {}

        # Load topic configurations
        self.load_config()

        # Set up publishers for each property
        for key in self.properties.keys():
            self.publishers[key] = rospy.Publisher(
                self.topics[key],
                String,
                queue_size=0
            )

        rospy.Service(
            self.services['set_property'],
            Set,
            self.set_property)

    def load_config(self):
        """Load topic names from the configuration file.

        This method reads the 'topics.yaml' configuration file and loads
        the names of the topics into the `self.topics` dictionary.

        @return: None
        """
        rospack = rospkg.RosPack()
        path = rospack.get_path("controller")

        # Load topics
        with open(path + "/config/topics.yaml", 'r') as file:
            self.topics = yaml.safe_load(file)

        # Load services
        with open(path + "/config/services.yaml", 'r') as file:
            self.services = yaml.safe_load(file)

    def publish(self):
        """Publishes the current authority and pattern.

        This method creates messages for the current authority and pattern,
        publishes them to their respective topics, and sleeps for a brief
        moment to regulate the publishing rate.

        @return: None
        """
        for key in self.properties.keys():
            msg = String()
            msg.data = self.properties[key]
            self.publishers[key].publish(msg)

    def set_property(self, req):
        """Service callback to set the property values.

        This method modifies the authority or pattern properties based on the
        incoming service request.

        @param req: The service request containing the property and value to set.
        @type req: Set.Request

        @return: True if the property was set successfully, False otherwise.
        @rtype: bool
        """
        if req.property.name.data in self.properties:
            self.properties[req.property.name.data] = req.property.value.data
            return True  # Indicate success

        return False  # Indicate failure if property is not recognized



def main():
    rospy.init_node("controller", anonymous=True)
    controller = Controller()

    # if(not rospy.has_param('~pattern')):
    #     rospy.loginfo(f"no pattern set\n Pattern set to multicolor_square")
    #     pattern = "multicolor_square"
    # else:
    #     pattern = rospy.get_param('~pattern')
    #
    # # Set authority
    # if(not rospy.has_param('~authority')):
    #     rospy.loginfo(f"no authority set\n Authority set to cobotic")
    #     auth = 'cobotic'
    # else:
    #     auth = rospy.get_param('~authority')
    #     if(auth != 'cobotic' and auth != 'human' and auth != "mixed"):
    #         rospy.loginfo(f"{auth} doesn't recognize\n Authority set to cobotic")
    #         auth = 'cobotic'





    r = rospy.Rate(1)
    while not rospy.is_shutdown():
        #Publish autority
        controller.publish()
        r.sleep()


if __name__ == "__main__":
    main()
