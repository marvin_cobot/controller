# Changelog

## [Unreleased]

### Added

- Root control. 2024-10-15.
- Read authority and pattern as rosrun parameters. 2024-10-15.
- Service to set authority and pattern. 2024-10-28.

### Changed
- Read authority and pattern as rosrun parameters. 2024-10-28.

### Fixed
