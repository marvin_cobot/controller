# State Controller

The **State Controller** is a ROS component designed to manage the state of blocks within a robotic system. It tracks the position and visibility of blocks based on cell state updates and publishes block positions to other control nodes. The controller initializes a goal state for each block and updates this state based on real-time cell data.

## Installation

### Prerequisites

- [Ubuntu 20.04](https://releases.ubuntu.com/20.04/)
- [ROS noetic](http://wiki.ros.org/noetic)
- [rospy](https://wiki.ros.org/rospy)
- [OpenCV](https://pypi.org/project/opencv-python/)
- [cv_bridge](http://wiki.ros.org/cv_bridge)
- [vision](https://gitlab.com/marvin_cobot/vision)

### Install Dependencies

To install the required dependencies, use:

```bash
sudo apt-get install ros-noetic-cv-bridge
pip install opencv-python
pip install pupil-apriltags
cd ~/catkin_ws/src && git clone https://gitlab.com/marvin_cobot/vision/vision
```
### Building the package

```bash
cd ~/catkin_ws/src
git clone https://gitlab.com/marvin_cobot/controller
cd ..
catkin build
source devel/setup.bash
```

## Configuration

The package includes configuration files that must be set up before running the node. All these files should be located within the **config** directory of the **state_controller** package.

### Vision Interface Configuration File

The **sensors.yaml** file defines sensor settings and topics for acquiring RGB images used in color and hand detection.

Example structure of sensors.yaml:
```yaml
color: /vision/vision_interface/cell_state
human_pose: /vision/vision_interface/human_pose
```
- See the [vision_inter](https://gitlab.com/marvin_cobot/vision) for more details.

### Table Configuration File

The **table.yaml** file specifies the dimensions of the platform in terms of the number of cells, both in width and height. This configuration allows the node to divide the detected platform area into a grid of cells accurately.

Example structure of platform_dimension.yaml:
```yaml
h_cells: 20
w_cells: 44
```
- **w_cells**: The number of cells along the platform's width.
- **h_cells**: The number of cells along the platform's height.

These values determine the grid layout used for dividing the platform into smaller sections (cells) and assigning coordinates to each cell in the grid.

### Topics Configuration File

The **topics.yaml** file defines the ROS topics used by nodes in the vision_interface package to publish data.

Example structure of topics.yaml:
```yaml
color: /controller/state_controller/color_controller/cell_state
state: /controller/state_controller/block_pose
goal: /controller/state_controller/goal
pattern: /controller/pattern
```
- **color**: The topic where cell's colors are puvblished after controlling hand position.
- **state**: The topic publishing the position of each block.
- **goal**: The topic publishing the goal position of each block.
- **pattern**: Pattern's name.

## Launch

```bash
roslaunch state_controller controller.launch
```

## Licence

This project is licensed under the LGPL License

## Maintainers

- [Maxence Grand](Maxence.Grand@univ-grenoble-alpes.fr), Research Engineer, Laboratoire D'informatique de Grenoble, Univ. Grenoble Alpes, Grenoble 38000 France
