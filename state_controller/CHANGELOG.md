# Changelog

## Legacy [2024-09]

- Real Time cube detection.

## [Unreleased]

### Added

- color control with hand position. 2024-09-16.
- Example pattern (simple, level_[1-4])
### Changed

- Assume non-visible block are unmoved. 2024-09-16.

### Fixed

- Phantom block. 2024-10-29.
