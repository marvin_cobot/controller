#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" A state controller to interprete perception low level data."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import rospy
import json, yaml
import os, rospkg
import copy

from state_controller.msg import Block, BlockState
from std_msgs.msg import String
from vision_interface.msg import CellState
from vision_driver.msg import Cell

rospack = rospkg.RosPack()
path = rospack.get_path("vision_interface")
COLORS = json.load(open(f"{path}/database/colors.json"))

def point_is_inside(point, block):
    """Check if a point is inside a block.

    @param point: A point with x, y coordinates.
    @param block: A block object with top_left and bottom_right attributes.
    @return: Boolean indicating if the point is within the block.
    """
    return point.x >= block.top_left.x and point.x <= block.bottom_right.x and\
        point.y >= block.top_left.y and point.y <= block.bottom_right.y

def is_inside(corners, block):
    """Check if any corner is inside a block.

    @param corners: Set of corners (top_left, top_right, bottom_left, bottom_right).
    @param block: Block within which corners may reside.
    @return: Boolean indicating if any corner is inside the block.
    """
    if(point_is_inside(corners.top_left, block)):
        return True
    if(point_is_inside(corners.top_right, block)):
        return True
    if(point_is_inside(corners.bottom_left, block)):
        return True
    if(point_is_inside(corners.bottom_right, block)):
        return True
    return False

def get_cell(cell_state, x, y):
    """Get the cell at specified coordinates.

    @param cell_state: CellState object containing cell information.
    @param x: The x-coordinate of the cell.
    @param y: The y-coordinate of the cell.
    @return: Cell object at (x, y) or None if not found.
    """
    for cell in cell_state.cells:
        if(cell.i == x and cell.j == y):
            return cell

def is_a_cube(corners):
    """Determine if the points form a cube.

    @param corners: List of corners of the shape.
    @return: Boolean indicating if the shape is a cube.
    """
    [top_left, top_right, bottom_left, bottom_right] = corners
    return top_left[0]+1 == bottom_right[0] and\
        top_left[1]+1 == bottom_right[1]

def is_a_brick(corners):
    """Determine if the points form a brick.

    @param corners: List of corners of the shape.
    @return: Boolean indicating if the shape is a brick.
    """
    [top_left, top_right, bottom_left, bottom_right] = corners
    return\
        (top_left[0]+3 == bottom_right[0] and\
            top_left[1]+1 == bottom_right[1]) or\
        (top_left[0]+1 == bottom_right[0] and\
            top_left[1]+3 == bottom_right[1])

def create_block_msg_from_json(block_json):
    """Create a Block message from JSON data.

    @param block_json: JSON object with block information.
    @return: Block message populated with the data.
    """
    block =  Block()

    block.id = block_json["id"]

    block.color = block_json["color"]

    block.top_left.x = block_json["pose"]["top"]["left"]["x"]
    block.top_left.y = block_json["pose"]["top"]["left"]["y"]
    block.top_left.z = block_json["pose"]["top"]["left"]["z"]

    block.bottom_left.x = block_json["pose"]["bottom"]["left"]["x"]
    block.bottom_left.y = block_json["pose"]["bottom"]["left"]["y"]
    block.bottom_left.z = block_json["pose"]["bottom"]["left"]["z"]

    block.top_right.x = block_json["pose"]["top"]["right"]["x"]
    block.top_right.y = block_json["pose"]["top"]["right"]["y"]
    block.top_right.z = block_json["pose"]["top"]["right"]["z"]

    block.bottom_right.x = block_json["pose"]["bottom"]["right"]["x"]
    block.bottom_right.y = block_json["pose"]["bottom"]["right"]["y"]
    block.bottom_right.z = block_json["pose"]["bottom"]["right"]["z"]

    block.visible = True
    return block

def get_cell_colors(cell_state, h_cells = 44, w_cells = 20):
    """Organize cells by color for easy reference.

    @param cell_state: CellState object containing cell colors.
    @param h_cells: Height of the cell grid.
    @param w_cells: Width of the cell grid.
    @return: Dictionary of color lists with cell positions.
    """
    cell_colors = {}
    for c in COLORS.keys():
        cell_colors[c] = []
    for i in range(h_cells):
        for j in range(w_cells):
            cell = get_cell(cell_state, i, j)
            if(cell.color in cell_colors.keys()):
                cell_colors[cell.color].append((i,j))
    return cell_colors

def extract_bricks(cell_colors):
    """Extract bricks from cell colors.

    @param cell_colors: Dictionary of cells organized by color.
    @return: Dictionary of brick positions organized by color.
    """
    bricks = {}
    for c in COLORS.keys():
        bricks[c] = []
    for color in bricks.keys():
        bricks[color].append(extract_brick(cell_colors, color))
    return bricks

def extract_brick(cell_colors, color):
    """Extract a brick of the specified color.

    @param cell_colors: Dictionary of cell positions organized by color.
    @param color: Color of the brick to extract.
    @return: List of coordinates for the brick or None.
    """
    for (x0,y0) in cell_colors[color]:
        #check if (x0,y0) in the top left of an horizontal brick
        bool_h = True
        for x in range(x0+1, x0+4):
            if(not bool_h):
                break
            for y in range(y0+1,y0+2):
                if((x,y) in cell_colors[color]):
                    continue
                else:
                    bool_h = False
                    break
        if bool_h:
            return [(x0+1,y0+1), (x0+4,y0+1), (x0+1,y0+2), (x0+4,y0+2)]
        #check if (x0,y0) in the top left of a vertical brick
        bool_v = True
        for x in range(x0+1, x0+2):
            if(not bool_v):
                break
            for y in range(y0+1,y0+4):
                if((x,y) in cell_colors[color]):
                    continue
                else:
                    bool_v = False
                    break
        if bool_v:
            return [(x0+1,y0+1), (x0+2,y0+1), (x0+2,y0+4), (x0+2,y0+4)]
    return None

def extract_cubes(cell_colors):
    """Extract cubes from cell colors.

    @param cell_colors: Dictionary of cell positions organized by color.
    @return: Dictionary of cube positions organized by color.
    """
    cubes = {}
    for c in COLORS.keys():
        cubes[c] = []
    for color in cubes.keys():
        cubes[color] = extract_cubes_color(cell_colors, color)
    return cubes

def extract_cubes_color(cell_colors, color):
    """Extract cubes of the specified color.

    @param cell_colors: Dictionary of cell positions organized by color.
    @param color: Color of cubes to extract.
    @return: List of cube positions.
    """
    cubes = []
    for (x0,y0) in cell_colors[color]:
        #check if (x0,y0) is a potential top left of a cube
        bool = True
        for x in range(x0+1, x0+2):
            if(not bool):
                break
            for y in range(y0+1,y0+2):
                if((x,y) in cell_colors[color]):
                    continue
                else:
                    bool = False
                    break
        if bool:
            potential = [(x0+1,y0+1), (x0+2,y0+1), (x0+1,y0+2), (x0+2,y0+2)]
            #A potential cube is a cube iff all neighbors color cells are different
            if(
                not ((x0-1,y0) in cell_colors[color]) and \
                not ((x0-1,y0+1) in cell_colors[color]) and \
                not ((x0,y0+2) in cell_colors[color]) and \
                not ((x0+1,y0+2) in cell_colors[color]) and \
                not ((x0+2,y0+1) in cell_colors[color]) and \
                not ((x0+2,y0) in cell_colors[color]) and \
                not ((x0+1,y0-1) in cell_colors[color]) and \
                not ((x0,y0-1) in cell_colors[color])
            ):
                cubes.append(potential)

    return cubes

def get_max_occurences(lst) :
    contents = []
    occurences = []

    for data in lst :
        new = True
        for i in range(len(contents)) :
            if data == contents[i] :
                occurences[i] += 1
                new = False
                break
        if new :
            contents.append(data)
            occurences.append(1)
    maxi = 0
    pose_max = -1
    for i in range(len(occurences)) :
        if(occurences[i]>maxi) :
            pose_max = i
            maxi = occurences[i]
    return contents[pose_max]




class StateController:
    """The State Controller manages the goal and block positions within a robotic system."""

    def __init__(self):
        """Initializes the State Controller, sets up subscribers, and initializes goal and block states."""
        self.pattern = "no_pattern"
        self.queue_size = 5
        self.parse_json()
        self.init_goal_state()
        self.init_block_poses()
        rospack = rospkg.RosPack()
        path = rospack.get_path("state_controller")
        self.topics = yaml.safe_load(open(f"{path}/config/topics.yaml"))
        rospy.Subscriber(
			self.topics['color'],
			CellState,
			self.upd_block_poses)
        rospy.Subscriber(
			self.topics['pattern'],
			String,
			self.read_pattern)
        rospy.loginfo("State Controller Node Initialized")
        self.pub_goal = rospy.Publisher('/controller/state_controller/goal', BlockState, queue_size=1)
        self.pub_goal.publish(self.goal)
        rospy.loginfo("/controller/state_controller/goal ready")

        self.pub_block = rospy.Publisher('/controller/state_controller/block_poses', BlockState, queue_size=1)
        self.pub_block.publish(self.block_poses)
        rospy.loginfo("/controller/state_controller/block_poses ready")

    def read_pattern(self, pattern):
        """Updates the pattern for assembly and reinitializes the goal and block states.

        @param pattern: The new assembly pattern, triggering a reinitialization if different from the current one.
        """
        if(self.pattern != pattern.data):
            self.pattern = pattern.data
            self.parse_json()
            self.init_goal_state()
            self.init_block_poses()

    def init_goal_state(self):
        """Initializes the goal state for blocks based on parsed JSON data.

        This function reads the goal configuration from JSON data and converts
        it into a BlockState message that represents the desired positions of blocks.
        """
        state = BlockState()
        state.blocks = []
        for block_json in self.data['goal']:
            state.blocks.append(create_block_msg_from_json(block_json))
        self.goal = state

    def init_block_poses(self):
        """Initializes the block positions for the system.

        Sets up the initial position of blocks based on the goal configuration.
        """
        state = BlockState()
        state.blocks = []
        for block_json in self.data['goal']:
            state.blocks.append(create_block_msg_from_json(block_json))
        self.block_poses = state
        self.queue = []
        for _ in range(self.queue_size) :
            self.queue.append(copy.deepcopy(self.block_poses))

    def upd_block_poses(self, cell_state):
        """Updates block positions based on cell state data.

        @param cell_state: A CellState message containing cell colors and positions.
        """
        cell_colors = get_cell_colors(cell_state)
        cubes = extract_cubes(cell_colors)

        #Check that the blocks are always in the same position
        #Otherwise, they are no longer considered “visible”
        for id_block in range(len(self.block_poses.blocks)):
            block = self.block_poses.blocks[id_block]
            corners = [
                (block.top_left.x, block.top_left.y),
                (block.top_right.x, block.top_right.y),
                (block.bottom_left.x, block.bottom_left.y),
                (block.bottom_right.x, block.bottom_right.y)
            ]

            shapes = cubes if is_a_cube(corners) else bricks
            visible = corners in shapes[block.color]

            block.visible = visible
            self.block_poses.blocks[id_block] = block

        #Checks that non-visible blocks have been moved
        #If they have been moved, their coordinates are modified
        #and they are considered visible.
        for id_block in range(len(self.block_poses.blocks)):
            block = self.block_poses.blocks[id_block]
            if(block.visible):
                continue

            corners = [
                (block.top_left.x, block.top_left.y),
                (block.top_right.x, block.top_right.y),
                (block.bottom_left.x, block.bottom_left.y),
                (block.bottom_right.x, block.bottom_right.y)
            ]

            shapes = cubes if is_a_cube(corners) else bricks

            if len(shapes[block.color]) > 0:
                # print('present in shape')
                #We assume that all bricks are unique
                new_corners = shapes[block.color][0]
                if(new_corners != None):
                    level = 0

                    block.visible = True
                    block.top_left.x = new_corners[0][0]
                    block.top_left.y = new_corners[0][1]
                    block.top_left.z = level
                    block.top_right.x = new_corners[1][0]
                    block.top_right.y = new_corners[1][1]
                    block.top_right.z = level
                    block.bottom_left.x = new_corners[2][0]
                    block.bottom_left.y = new_corners[2][1]
                    block.bottom_left.z = level
                    block.bottom_right.x = new_corners[3][0]
                    block.bottom_right.y = new_corners[3][1]
                    block.bottom_right.z = level
                    self.block_poses.blocks[id_block] = block

        #Check 3D
        #Check if a non-visible block is below
        for id_block in range(len(self.block_poses.blocks)):
            block = self.block_poses.blocks[id_block]
            if(not block.visible):
                continue
            #Reinit z to 0
            block.top_left.z = 0
            block.top_right.z = 0
            block.bottom_left.z = 0
            block.bottom_right.z = 0
            for id_block2 in range(len(self.block_poses.blocks)):
                if(id_block == id_block2):
                    continue
                block2 = self.block_poses.blocks[id_block2]
                if(block2.visible):
                    continue
                if(is_inside(block, block2) and\
                            block.top_left.z <= block2.top_left.z):
                    block.top_left.z = block2.top_left.z + 1
                    block.top_right.z = block2.top_right.z + 1
                    block.bottom_left.z = block2.bottom_left.z + 1
                    block.bottom_right.z = block2.bottom_right.z + 1
                    print(f"{block.color} is on {block2.color}")


        self.queue[0:self.queue_size-1] = self.queue[1:self.queue_size]
        self.queue[self.queue_size-1] = copy.deepcopy(self.block_poses)

        for id_block in range(len(self.block_poses.blocks)):
            colors = []
            poses = []
            visibles = []
            for id_queue in range(self.queue_size) :
                block = self.queue[id_queue].blocks[id_block]
                corners = [
                    (block.top_left.x, block.top_left.y, block.top_left.z),
                    (block.top_right.x, block.top_right.y, block.top_right.z),
                    (block.bottom_left.x, block.bottom_left.y, block.bottom_left.z),
                    (block.bottom_right.x, block.bottom_right.y, block.bottom_right.z)
                ]

                colors.append(block.color)
                poses.append(corners)
                visibles.append(block.visible)

            block = self.block_poses.blocks[id_block]

            block.color= get_max_occurences(colors)
            corner = get_max_occurences(poses)
            block.top_left.x = corner[0][0]
            block.top_left.y = corner[0][1]
            block.top_left.z = corner[0][2]
            block.top_right.x = corner[1][0]
            block.top_right.y = corner[1][1]
            block.top_right.z = corner[1][2]
            block.bottom_left.x = corner[2][0]
            block.bottom_left.y = corner[2][1]
            block.bottom_left.z = corner[2][2]
            block.bottom_right.x = corner[3][0]
            block.bottom_right.y = corner[3][1]
            block.bottom_right.z = corner[3][2]
            block.visible = get_max_occurences(visibles)

            self.block_poses.blocks[id_block] = block



        self.pub_block.publish(self.block_poses)
        self.pub_goal.publish(self.goal)

    def parse_json(self):
        """Parses the JSON model file and loads data for the current pattern.

        Retrieves the file path from the package, reads model data, and stores it
        in self.data based on the current pattern.
        """

        rospack = rospkg.RosPack()
        path = rospack.get_path("state_controller")
        json_file = f"{path}/database/models.json"
        self.data = json.load(open(json_file))[self.pattern]

def main():
    rospy.init_node("state_controller", anonymous=True)
    controller = StateController()
    rospy.spin()


if __name__ == "__main__":
    main()
