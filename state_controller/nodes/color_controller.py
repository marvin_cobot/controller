from __future__ import print_function

""" A color controller"""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

WIDTH=1440
HEIGHT=720

import rospy
import json, yaml
import os, rospkg

from vision_interface.msg import CellState, HumanPose
from vision_driver.msg import Cell

import cv2
import math

global frame

def get_bgr(rgb):
	"""Convert RGB color format to BGR.

    Reverses the order of color values to convert RGB to BGR.

    @param rgb: List of RGB color values.
    @return: List of BGR color values.
    """
	c = []
	i = len(rgb)-1
	while i >= 0:
		c.append(rgb[i])
		i-=1
	c.append(255)
	return c

class ColorController:
	"""The color controller.

    This class is responsible for visualizing the state of cells in a table.
    It subscribes to topics related to cell states and human poses, processes
    the information, and publishes the updated cell states. The visualization
    is done using OpenCV.

    Attributes:
        has_frame (bool): Indicates whether the frame has been updated.
        frame (numpy.ndarray): The background image used for drawing cell states.
        colors (dict): A dictionary of colors loaded from a JSON file.
        table (dict): Configuration data for the table loaded from a YAML file.
        topics (dict): Configuration data for the topics loaded from a YAML file.
        vision_interface (dict): Configuration data for the vision interface loaded from a YAML file.
        human_pose (HumanPose): The current human pose data.
        cell_state (CellState): The current state of cells.
        pub_cell_state (rospy.Publisher): Publisher for the cell state topic.
    """

	def __init__(self):
		"""Constructs the color controller.

        Initializes the frame, colors, table configuration, topics, and subscribes to cell_state ROS topics.
        """
		self.has_frame = False
		rospack = rospkg.RosPack()
		path = rospack.get_path("state_controller")
		self.frame = cv2.imread(f"{path}/database/background.png")
		self.colors = json.load(open(f"{path}/database/colors.json"))
		self.table = yaml.safe_load(open(f"{path}/config/table.yaml"))
		self.topics = yaml.safe_load(open(f"{path}/config/topics.yaml"))
		self.vision_interface =\
			yaml.safe_load(open(f"{path}/config/vision_interface.yaml"))
		rospy.loginfo(f"color_controller subscribes to the {self.vision_interface['color']}")
		rospy.Subscriber(
			self.vision_interface['color'],
			CellState,
			self.callback_cell_state)
		rospy.loginfo(f"color_controller subscribes to the {self.vision_interface['human_pose']}")
		rospy.Subscriber(
			self.vision_interface['human_pose'],
			HumanPose,
			self.callback_human_pose)
		self.human_pose = HumanPose()
		self.cell_state = CellState()
		self.pub_cell_state = rospy.Publisher(
			self.topics['color'],
			CellState,
			queue_size=1
		)

	def publish(self):
		"""Publish the current cell state.

        This method publishes the current cell state

        @return: None
        @rtype: NoneType
        """
		self.pub_cell_state.publish(self.cell_state)

	def callback_cell_state(self, cells):
		"""Callback when new cell state is received.

        This method processes the received cell state, updates the colors of the cells
        based on human poses, and publishes the updated state.

        @param cells: The new cell state received.
        @type cells: CellState

        @return: None
        @rtype: NoneType
		"""
		self.cell_state = cells
		for c1 in self.cell_state.cells:
			x1 = c1.i
			y1 = c1.j
			for h in self.human_pose.hands:
				for c2 in h.occupied:
					x2 = c2.i
					y2 = c2.j
					if(x1 == x2 and y1 == y2):
						c1.color = 'undefined'
				for c2 in h.possibly_occupied:
					x2 = c2.i
					y2 = c2.j
					if(x1 == x2 and y1 == y2):
						c1.color = 'undefined'
		self.publish()
		# self.show_state(self.cell_state)

	def callback_human_pose(self, human_pose):
		"""Callback when new human pose is received.

        This method updates the current human pose with the received data.

        @param human_pose: The new human pose data received.
        @type human_pose: HumanPose

        @return: None
        @rtype: NoneType
        """
		self.human_pose = human_pose

	def show_state(self, state):
		"""Draw the cell state, i.e. draw all cells with their color.

        This method visualizes the state of the cells on the background frame,
        drawing each cell based on its color.

		Cells' color for whose they are hidden by the the human are set to undefined.

        @param state: The current state of cells to be drawn.
        @type state: CellState

		@return: None
        @rtype: NoneType
        """

		#draw table
		for cell in state.cells:

			x = cell.i
			y = cell.j

			start = (
				int((x/self.table['w_cells']) * WIDTH),
				int((y/self.table['h_cells']) * HEIGHT)
			)

			end = (
				int(((x + 1)/self.table['w_cells']) * WIDTH),
				int(((y + 1)/self.table['h_cells']) * HEIGHT)
			)

			if(cell.color == 'undefined'):
				self.frame = cv2.rectangle(self.frame, start, end, [0,0,0,100], -1)
				self.frame = cv2.rectangle(self.frame, start, end, [0,0,0,255], 2)
				self.frame = cv2.line(self.frame, start, end, [0,0,255,255], 5)
			else:
				c = get_bgr(self.colors[cell.color]['RGB'])
				self.frame = cv2.rectangle(self.frame, start, end, c, -1)
				self.frame = cv2.rectangle(self.frame, start, end, [0,0,0,255], 2)
		self.has_frame = True

def main():
	rospy.init_node("color_controller", anonymous=True)
	controller = ColorController()
	rospy.loginfo('Node color_controller initialized')

	rospy.spin()

if __name__ == "__main__":
	main()
