from __future__ import print_function

""" A visualization tool for block states."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import rospy
import json
import os, rospkg

from state_controller.msg import Block, BlockState

import cv2
import math

WIDTH=1440
HEIGHT=720

BLACK = (0, 0, 0, 255)
DARK_GREY = (111, 111, 111, 255)
RED = (0, 0, 255, 255)
GREEN = (0, 255, 0, 255)
BLUE = (255, 0, 0, 255)
PURPLE = (255, 0, 255, 255)
YELLOW = (0, 255, 255, 255)
COLOR = {"red":RED, "blue":BLUE, "yellow":YELLOW, "green":GREEN, "dark_grey":DARK_GREY}

def get_bgr(rgb):
	c = []
	i = len(rgb)-1
	while i >= 0:
		c.append(rgb[i])
		i-=1
	c.append(255)
	return c

class Viewer:
    def __init__(self):
        self.has_frame = False
        rospack = rospkg.RosPack()
        self.path = rospack.get_path("state_controller")
        self.frame = cv2.imread(f"{self.path}/database/background.png")
        rospack = rospkg.RosPack()
        path = rospack.get_path("vision_interface")
        self.colors = json.load(open(f"{path}/database/colors.json"))
        rospy.Subscriber(
            "/controller/state_controller/block_poses",
            BlockState,
            self.show_state)

    def show_state(self, state):
        #draw table
        self.frame = cv2.imread(f"{self.path}/database/background.png")

        #draw cells
        for i in range(44):
            cv2.line(
                self.frame,
                (int(i*(WIDTH/44)), 0),
                (int(i*(WIDTH/44)), HEIGHT),
                (0, 0, 0),
                1)
        for j in range(20):
            cv2.line(
            self.frame,
            (0, int(j*(HEIGHT/20))),
            (WIDTH, int(j*(HEIGHT/20))),
            (0, 0, 0),
            1)

        #draw blocks
        for level in [0,1,2,3,4]:
            for block in state.blocks:
                if(block.top_left.z != level):
                    continue
                start = [
                    int(((block.top_left.x-1)/44)*WIDTH),
                    int(((block.top_left.y-1)/20)*HEIGHT)
                ]
                end = [
                    int((block.bottom_right.x/44)*WIDTH),
                    int((block.bottom_right.y/20)*HEIGHT)
                ]
                middle = [
                    int((start[0]+end[0])/2),
                    int((start[1]+end[1])/2)
                ]
                # print(frame.shape)
                # print(end)
                if(block.visible):
                    fillcolor = get_bgr(self.colors[block.color]['RGB'])
                else:
                    fillcolor = DARK_GREY

                self.frame = cv2.rectangle(self.frame, start, end, fillcolor, -1)
                self.frame = cv2.putText(
                    self.frame,
                    f"{int(block.bottom_left.z)}",
                    middle,
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1,
                    (0,0,0,255),
                    2,
                    cv2.LINE_AA)
        self.has_frame = True

def main():
    rospy.init_node("state_visualization", anonymous=True)
    viewer = Viewer()
    r = rospy.Rate(30)
    while not rospy.is_shutdown():
        if not viewer.has_frame:
            r.sleep()
        else:
            cv2.imshow("block_poses", viewer.frame)
            r.sleep()
            if(cv2.waitKey(33) & 0xFF in (ord('q'),27)):
                cv2.destroyAllWindows()
                break

if __name__ == "__main__":
    main()
