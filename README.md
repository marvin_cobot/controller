# Controller

>This repo is a fork of this [repo](https://gricad-gitlab.univ-grenoble-alpes.fr/pacbot_group/state_controller)

>This module is part of the [Human-ABB Yumi Collaboration Project](https://gitlab.com/marvin_cobot/controller). For more details of all the modules required to use this module, please refer to this project.

## Overview

The controller is the central ROS module This module enables communication with other ROS modules (Perception, Agents, Planning). It consists of two sub-modules: the state_controller, which provides a high-level interpretation of data from the perception module, and the task_controller, which, through the planning module, schedules tasks among agents.

## Sub packages

### State Controller

The [State Controller](state_controller/README.md) is a ROS component designed to manage the state of blocks within a robotic system. It tracks the position and visibility of blocks based on cell state updates and publishes block positions to other control nodes. The controller initializes a goal state for each block and updates this state based on real-time cell data.

### Task Controller

The [Task Controller](task_controller/README.md) is a component of a robotic assembly system that manages the state and behaviors of various blocks within the assembly environment. It monitors the positions of blocks, checks their conformity to predefined goals, and handles error reporting for any discrepancies found during the assembly process.

## Installation

### Dependencies

This module depends on the following packages:

- [Ubuntu 20.04](https://releases.ubuntu.com/20.04/)
- [ROS noetic](http://wiki.ros.org/noetic)
- [rospy](https://wiki.ros.org/rospy)
- [rosplan](https://gitlab.com/marvin_cobot/rosplan.git)

Make sure you have installed these dependencies before proceeding.

### Installation from Source

1. Clone this repository into your ROS workspace:

```bash
cd path/to/your/ros/workspace/src
git clone https://gitlab.com/marvin_cobot/controller.git
```

2. Build the package using catkin:

```bash
cd path/to/your/ros/workspace
catkin build
```

## Usage

### Getting started

Before to start, follow these steps:

1. Ensure that the ROS master (roscore) is running:

```bash
roscore
roslaunch vision vision.launch
```

2. Launch each subpackages

```bash
roslaunch controller controller.launch
roslaunch state_controller controller.launch
roslaunch task_controller controller.launch
```

3. Setting controllers property

- Setting pattern to assembly
```bash
rosservice call /controller/set_property
  "name:
    data: 'pattern'
  pattern:
    data: 'level_1'"
```
- Setting authority
```bash
rosservice call /controller/set_property
  "name:
    data: 'authority'
  pattern:
    data: 'human'"
```

## Licence

This project is licensed under the LGPL License

## Maintainers

- [Maxence Grand](Maxence.Grand@univ-grenoble-alpes.fr), Research Engineer, Laboratoire D'informatique de Grenoble, Univ. Grenoble Alpes, Grenoble 38000 France
