# Changelog

## Legacy [2024-09]

- Real Time interaction with ROS PDDL Planner node with universal and existential quantifier.

## [Unreleased]

### Added

- Only cube block assume. 2024-10-09.
- Logical representation of state. 2024-10-09.
- Adding plan checking. 2024-10-11.
- Plan with empty assembly zone. 2024-10-11.
- Planning with agent position (action cost). 2024-10-11.
- Replanning. 2024-11-10.
- error_handler. 2024-10-21.
- Scheduler for sharing action between agents. 2024-10-15.
- End problem handler. 2024-10-30.

### Changed

- Remove quantifier. 2024-10-09.
- Remove empty assumption for 2D only. 2024-10-11.
- Action definition: add flag for done, feasible, error. 2024-10-29
- Scheduler check action feasibility. 2024-10-29.
- Integrate lifecycle. 2024-10-30.

### Fixed

- 3D planning/replanning. Add initial position + replan with wrong base. 2024-10-29.
- Wrong first stack block. 2024-10-31.
- Reduce search time. 2024-10-31.
