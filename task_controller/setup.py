#!/usr/bin/env python3

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup(
    packages=['task_controller'],
    package_dir={'': 'nodes'},
    requires=['rospy', 'std_msgs', 'state_controller', 'rosplan']
)

setup(**setup_args)
