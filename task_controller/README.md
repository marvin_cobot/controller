# Task COntroller

The Task Controller is a component of a robotic assembly system that manages the state and behaviors of various blocks within the assembly environment. It monitors the positions of blocks, checks their conformity to predefined goals, and handles error reporting for any discrepancies found during the assembly process.

## Installation

### Prerequisites

- [Ubuntu 20.04](https://releases.ubuntu.com/20.04/)
- [ROS noetic](http://wiki.ros.org/noetic)
- [rospy](https://wiki.ros.org/rospy)
- [OpenCV](https://pypi.org/project/opencv-python/)
- [cv_bridge](http://wiki.ros.org/cv_bridge)
- [vision](https://gitlab.com/marvin_cobot/vision)
- [State Controller](https://gitlab.com/marvin_cobot/controller/state_controller)

### Install Dependencies

To install the required dependencies, use:

```bash
sudo apt-get install ros-noetic-cv-bridge
pip install opencv-python
pip install pupil-apriltags
cd ~/catkin_ws/src && git clone https://gitlab.com/marvin_cobot/vision/vision
```
### Building the package

```bash
cd ~/catkin_ws/src
git clone https://gitlab.com/marvin_cobot/controller
cd ..
catkin build
source devel/setup.bash
```

## Configuration

The package includes configuration files that must be set up before running the node. All these files should be located within the **config** directory of the **task_controller** package.

### Agents Configugarion FIle

The **agent.yaml** file specifies the characteristics and positions of various agents involved in the assembly process. Below is an example of how to define an operator in this configuration file.

```yaml
operator:
  type: human
  position: south
```

- **operator**: This key defines the characteristics of the "operator agent" interacting with the assembly system.
  - **type**: Specifies the type of operator. In this example, it is set to human, indicating that a human operator will control the assembly process.
  - **position**: Defines the position of the operator relative to the assembly area. Here, it is set to south, which indicates that the operator is located on the southern side of the assembly zone. This can be adjusted based on the layout of your workspace.

### Services Configuration File

The **services.yaml** file configures the various services used by the Task Controller, including the planner and checker services. This setup allows for efficient task planning and verification in the robotic assembly process.

```yaml
planner:
  pddl: /rosplan/solve_pddl
  adhoc: /controller/task_controller/ad_hoc_planner
  checker: /controller/task_controller/check_plan
```
- **planner**: This key encapsulates the configuration for the planning services.
  - **pddl**: The service endpoint for solving PDDL (Planning Domain Definition Language) problems.
  - **adhoc**: This service endpoint, set to /controller/task_controller/ad_hoc_planner, is used for on-the-fly or ad-hoc planning tasks. This allows for dynamic adjustments to the assembly process based on real-time data or changes in requirements.
  - **checker**: The endpoint for the plan checking service, configured to /controller/task_controller/check_plan. This service verifies the proposed plans against the current state of the system, ensuring that the tasks are feasible and conform to the defined goals.

### States Configuration File

The **states.yaml** file defines the various state topics that the Task Controller uses to communicate with other components of the robotic assembly system. Each topic corresponds to specific data related to the state of blocks and the assembly process.

```yaml
block_poses: /controller/state_controller/block_poses
goal: /controller/state_controller/goal
human_poses: /controller/state_controller/human_poses
```

- **block_poses**: The topic /controller/state_controller/block_poses provides the current positions and states of all blocks in the assembly environment.
- **goal**: The topic /controller/state_controller/goal defines the target configuration of blocks that the assembly system aims to achieve.
- **human_poses**: This topic, /controller/state_controller/human_poses, reports the positions and states of human operators in the workspace.

### Workplace zones Configuration File


The **zones.yaml** file defines the spatial zones used within the robotic assembly environment. These zones help the Task Controller to manage the positions of blocks and ensure that they are placed within specified areas for correct operation.

```yaml
assembly:
  top_left:
    x: 14
    y: 2
  top_right:
    x: 30
    y: 2
  bottom_left:
    x: 14
    y: 14
  bottom_right:
    x: 30
    y: 14
```

- **assembly**: This section defines the coordinates for the assembly zone where blocks can be placed during the assembly process.
  - **top_left**: The coordinates (x, y) of the top-left corner of the assembly zone.
  - **top_right**: The coordinates (x, y) of the top-right corner of the assembly zone.
  - **bottom_left**: The coordinates (x, y) of the bottom-left corner of the assembly zone.
  - **bottom_right**: The coordinates (x, y) of the bottom-right corner of the assembly zone.

### Lifecycle Configuration File

The **lifecycle.yaml** file provides the topics providing the different properties defining the lifecycle of the experiment.

```yaml
authority: /controller/authority
started: /start_signal
```

- **authority**: The authority of the experiment. Replanning is allowed if and only if the authority is set as **cobotic**.
- **started**: The planning process starts while true is read in this topic. 

### Topics Configuration File

The **topics.yaml** file defines the communication channels used by the Task Controller for publishing and subscribing to various topics related to the assembly process.


```yaml
plan: /controller/task_controller/plan
agent_plan: /controller/task_controller/plan_agent
error: /controller/task_controller/errors
```
- **plan**: This topic is used to publish the overall plan for the assembly task, allowing the Task Controller to communicate the necessary steps to achieve the desired configuration of blocks.
- **agent_plan**: This topic is specifically used for communication with individual agents.
  - **Note**: Replace agent in the topic name with the name of the specific agent defined in your agents.yaml file. For example, if you have a human agent defined as operator, the topic would be /controller/task_controller/plan_operator.
- **error**: This topic is used to publish any errors encountered during the assembly process, allowing for effective error handling and reporting.

## Launch

```bash
roslaunch task_controller controller.launch
```

## Licence

This project is licensed under the LGPL License

## Maintainers

- [Maxence Grand](Maxence.Grand@univ-grenoble-alpes.fr), Research Engineer, Laboratoire D'informatique de Grenoble, Univ. Grenoble Alpes, Grenoble 38000 France
