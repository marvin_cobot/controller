#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" An error handler."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import rospy
import os, rospkg, yaml

from state_controller.msg import Block, BlockState
from task_controller.msg import Errors

ZONES_CONFIG_FILE = "config/zones.yaml"
STATES_CONFIG_FILE = "config/states.yaml"
TOPICS_CONFIG_FILE = "config/topics.yaml"

def get_block_from_id(block_state, id):
    """
    Retrieve a block from the block state using its unique identifier.

    This function searches through the list of blocks in the provided block state
    and returns the block that matches the given identifier.

    @param block_state: An instance of the BlockState class containing a collection
                        of blocks.
    @type block_state: BlockState
    @param id: The unique identifier of the block to be retrieved.
    @type id: str
    @return: The block that matches the given id, or None if no block
             with that id is found.
    @rtype: Block or None
    """
    for b in block_state.blocks:
        if b.id == id:
            return b
    return None

class ErrorsHandler:
    """
    A class to handle and report errors in block assembly.

    The ErrorsHandler subscribes to relevant ROS topics to receive updates
    about the current state of blocks and their goal positions. It checks
    whether blocks are located within the designated assembly area and
    if they match the specified goal states. Any discrepancies are
    published as errors for further processing.
    """
    def __init__(self):
        """Constructs the ErrorsHandler and subscribes to relevant topics."""
        self.block_poses = BlockState()
        self.goal = BlockState()

        self.load_config()
        rospy.loginfo(f"Errors handler subscribes to the {self.states_topics['block_poses']} topic")
        rospy.Subscriber(
            self.states_topics['block_poses'],
            BlockState,
            self.callback_state)

        rospy.loginfo(f"Errors handler subscribes to the {self.states_topics['goal']} topic")
        rospy.Subscriber(
            self.states_topics['goal'],
            BlockState,
            self.callback_goal)

        self.block_errors = []

        self.publisher = rospy.Publisher(
            self.pub_topics['error'],
            Errors,
            queue_size=0)

        self.publish()


    def load_config(self):
        """Load configuration files for states and agents.

        This method reads YAML configuration files that define the state topics and agents
        required for the Plan checking process.
        """
        rospack = rospkg.RosPack()
        path = rospack.get_path("task_controller")

        rospy.loginfo("Read States Config")
        with open(path + "/" + STATES_CONFIG_FILE, 'r') as file:
            self.states_topics = yaml.safe_load(file)

        rospy.loginfo("Read Zones Config")
        with open(path + "/" + ZONES_CONFIG_FILE, 'r') as file:
            self.zones = yaml.safe_load(file)

        rospy.loginfo("Read Topics Config")
        with open(path + "/" + TOPICS_CONFIG_FILE, 'r') as file:
            self.pub_topics = yaml.safe_load(file)

    def is_in_assembly(self, block):
        """
        Check if a block is within the designated assembly area.

        @param block: The block to check.
        @type block: Block
        @return: True if the block is within the assembly area, otherwise False.
        @rtype: bool
        """
        return\
            block.top_left.x >= self.zones["assembly"]["top_left"]["x"] and\
            block.top_left.y >= self.zones["assembly"]["top_left"]["y"] and\
            block.bottom_left.x >= self.zones["assembly"]["bottom_left"]["x"] and\
            block.bottom_left.y <= self.zones["assembly"]["bottom_left"]["y"] and\
            block.top_right.x <= self.zones["assembly"]["top_right"]["x"] and\
            block.top_right.y >= self.zones["assembly"]["top_right"]["y"] and\
            block.bottom_right.x <= self.zones["assembly"]["bottom_right"]["x"] and\
            block.bottom_right.y <= self.zones["assembly"]["bottom_right"]["y"]

    def is_in_goal_pose(self, block, goal):
        """
        Check if a block matches the goal position.

        @param block: The block to check.
        @type block: Block
        @param goal: The goal position to compare against.
        @type goal: Block
        @return: True if the block matches the goal position, otherwise False.
        @rtype: bool
        """
        if(not goal):
            return False
        return\
            block.top_left.x == goal.top_left.x and\
            block.top_left.y == goal.top_left.y and\
            block.top_left.z == goal.top_left.z and\
            block.bottom_left.x == goal.bottom_left.x and\
            block.bottom_left.y == goal.bottom_left.y and\
            block.bottom_left.z == goal.bottom_left.z and\
            block.top_right.x == goal.top_right.x and\
            block.top_right.y == goal.top_right.y and\
            block.top_right.z == goal.top_right.z and\
            block.bottom_right.x == goal.bottom_right.x and\
            block.bottom_right.y == goal.bottom_right.y and\
            block.bottom_right.z == goal.bottom_right.z

    def check_blocks(self):
        """
        Check the positions of blocks and identify any errors.

        This method verifies whether blocks are within the assembly area and
        if they match the specified goal states. If a block is in the assembly
        zone but not in the goal position, its identifier is added to the error list.
        """
        errors = []
        for block in self.block_poses.blocks:
            if(self.is_in_assembly(block) and\
                not self.is_in_goal_pose(
                    block,
                    get_block_from_id(self.goal, block.id))):
                errors.append(block.id)

        self.block_errors = errors

    def publish(self):
        """Publish the list of block errors."""
        self.publisher.publish(self.block_errors)

    def callback_goal(self, goal):
        """
        Callback function for receiving the goal state.

        @param goal: The new goal state.
        @type goal: BlockState
        """
        self.goal = goal

    def callback_state(self, state):
        """
        Callback function for receiving the current block state.

        @param state: The current block state.
        @type state: BlockState
        """
        self.block_poses = state
        self.check_blocks()
        self.publish()

def main():
    rospy.init_node("errors_handler", anonymous=True)
    ErrorsHandler()
    rospy.spin()

if __name__ == "__main__":
    main()
