#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

"""Node that checks if the assembly goal has been reached.."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import rospy
import yaml
from state_controller.msg import BlockState
from std_msgs.msg import Bool
import rospkg

class EndChecker:
    """Node that checks if the assembly goal has been reached."""

    def __init__(self):
        """Initialize the EndChecker node."""
        rospy.init_node("end_checker", anonymous=True)

        # Load configuration to get the topic addresses
        self.load_config()

        # Subscribe to block poses and goal topics
        rospy.Subscriber(self.states_topics['block_poses'], BlockState, self.block_pose_callback)
        rospy.Subscriber(self.states_topics['goal'], BlockState, self.goal_callback)

        # Publisher for finish_assembly topic
        self.finish_assembly_pub = rospy.Publisher(self.topics['finish'], Bool, queue_size=10)

        self.current_block_state = BlockState()
        self.current_goal_state = BlockState()

    def load_config(self):
        """Load configuration files for states and topics."""
        rospack = rospkg.RosPack()
        path = rospack.get_path("task_controller")

        # Load states configuration
        rospy.loginfo("Reading States Config")
        with open(path + "/config/states.yaml", 'r') as file:
            self.states_topics = yaml.safe_load(file)

        # Load topics configuration
        rospy.loginfo("Reading Topics Config")
        with open(path + "/config/topics.yaml", 'r') as file:
            self.topics = yaml.safe_load(file)

    def block_pose_callback(self, block_poses):
        """Callback to process block poses and check if goal is reached."""
        self.current_block_state = block_poses
        self.check_finish_assembly()

    def goal_callback(self, goal):
        """Callback to process goal state."""
        self.current_goal_state = goal
        self.check_finish_assembly()

    def check_finish_assembly(self):
        """Check if the current block state matches the goal state."""
        if self.is_goal_reached():
            self.publish_finish_status(True)
        else:
            self.publish_finish_status(False)

    def is_goal_reached(self):
        """Determine if the current block poses match the goal."""
        if len(self.current_block_state.blocks) != len(self.current_goal_state.blocks):
            return False

        # Check if each block in the current state is in the goal state
        for block in self.current_block_state.blocks:
            goal_block = self.get_block_from_id(self.current_goal_state, block.id)
            if not goal_block or not self.are_blocks_equal(block, goal_block):
                return False

        return True

    def get_block_from_id(self, block_state, block_id):
        """Retrieve a block from the block state using its unique identifier."""
        for block in block_state.blocks:
            if block.id == block_id:
                return block
        return None

    def are_blocks_equal(self, block1, block2):
        """Check if two blocks have the same position."""
        return (
            block1.top_left == block2.top_left and
            block1.bottom_left == block2.bottom_left and
            block1.top_right == block2.top_right and
            block1.bottom_right == block2.bottom_right
        )

    def publish_finish_status(self, status):
        """Publish the finish assembly status."""
        finish_msg = Bool()
        finish_msg.data = status
        self.finish_assembly_pub.publish(finish_msg)

if __name__ == "__main__":
    try:
        end_checker = EndChecker()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
