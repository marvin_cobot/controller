#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" Ad hoc planner."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import rospy
import json
import os, rospkg, yaml

from state_controller.msg import Block, BlockState
from task_controller.msg import Pick, Place, PickNPlace, Plan
from rosplan.msg import PddlPlan, PddlAction, PddlProblem
from rosplan.srv import SolvePddlCmd, SolvePddlCmdRequest
from task_controller.srv import AdHocPlanCmd
from geometry_msgs.msg import Point
from task_controller.logical_state import BlockFactory, State, StateException

STATES_CONFIG_FILE = "config/states.yaml"
AGENTS_CONFIG_FILE = "config/agents.yaml"

class Planner:
    """The ad hoc planner class.

    This class represents an ad hoc planner used for task planning based on block states
    and goals. It subscribes to ROS topics for block poses and goals, and provides a service
    to generate ad hoc plans to fix positioning errors of blocks.
    """
    def __init__(self):
        """Constructs an Ad Hoc Planner.

        Initializes the planner by loading configuration files for states and agents,
        and preparing data structures for block poses, goals, and plans.
        """
        self.block_poses = BlockState()
        self.goal = BlockState()
        self.load_config()
        self.pddl_plan = PddlPlan()
        self.plan = Plan()

        rospy.loginfo(f"Ad Hoc planner subscribes to the {self.states_topics['block_poses']} topic")
        rospy.Subscriber(
            self.states_topics['block_poses'],
            BlockState,
            self.read_block_poses)

        rospy.loginfo(f"Ad Hoc planner subscribes to the {self.states_topics['goal']} topic")
        rospy.Subscriber(
            self.states_topics['goal'],
            BlockState,
            self.read_goal)
        rospy.Service(\
            '/controller/task_controller/ad_hoc_planner',\
            AdHocPlanCmd,
            self.ad_hoc_planning_service)

    def load_config(self):
        """Load configuration files for states and agents.

        This method reads YAML configuration files that define the state topics and agents
        required for the PDDL planning process.
        """
        rospack = rospkg.RosPack()
        path = rospack.get_path("task_controller")

        rospy.loginfo("Read States Config")
        with open(path + "/" + STATES_CONFIG_FILE, 'r') as file:
            self.states_topics = yaml.safe_load(file)

        rospy.loginfo("Agents Config")
        with open(path + "/" + AGENTS_CONFIG_FILE, 'r') as file:
            self.agents = yaml.safe_load(file)

    def read_block_poses(self, block_poses):
        """Read and update the current block poses.

        Updates the internal representation of block poses with the provided data.

        @param block_poses: The new block poses to be set.
        """
        self.block_poses = block_poses

    def read_goal(self, goal):
        """Read and update the goal block poses.

        Updates the internal representation of the goal with the provided data.

        @param goal: The new goal block poses to be set.
        """
        self.goal = goal

    def search_in_both_initial_goal(self, initial, goal):
        """Search for a block present in both the initial and goal position.

        This method identifies a block that is present in both the initial and goal
        positions but whose exact positions are different.

        @param initial: The initial state of the blocks.
        @type initial: State
        @param goal: The goal state of the blocks.
        @type goal: State
        @return: A block with position errors, or None if no such block is found.
        @rtype: Block or None
        """
        for block in initial.blocks:
            block_goal = goal.get_block_from_id(block.id)
            if(block_goal.position.is_overlap(block.position)):
                if(block.position.top_left.z == block_goal.position.top_left.z):
                    if(block.position != block_goal.position):
                        return block
        return None


    def search_inversed_stack(self, initial, goal):
        for block in initial.blocks:
            block_goal = goal.get_block_from_id(block.id)
            if(block_goal.position.is_overlap(block.position)):
                if(block.get_z() != block_goal.get_z()):
                    return block
        return None

    def search_overlap_with_goal(self, initial, goal):
        """Searches for blocks whose current position overlaps with the goal position.

        This function detects mispositioned blocks by identifying those whose position
        overlaps with the goal position.

        @param initial: The initial state of the blocks.
        @type initial: State
        @param goal: The target (goal) state of the blocks.
        @type goal: State
        @return: The mispositioned blocks overlapping the goal position.
        @rtype: list[Block]
        """
        res = []
        rospy.loginfo(initial.blocks)
        for block in initial.blocks:
            rospy.loginfo(f"Test {block}")
            if(block.get_z() == 0):
                pos = block.position
                id = block.id
                for other_block in goal.blocks:
                    if(other_block.get_z() > 0):
                        continue
                    rospy.loginfo(f"Other {other_block}")
                    other_pos = other_block.position
                    other_id = other_block.id
                    if(id == other_id):
                        rospy.loginfo(f"{pos.is_overlap(other_pos)} {pos != other_pos}")
                        if(pos != other_pos and pos.is_overlap(other_pos)):
                            rospy.loginfo(f"X add {block}")
                            res.append(block)
                            while(not initial.is_clear_block(block)):
                                block = initial.get_block_above(block)
                                res.append(block)
                                rospy.loginfo(f"Y add {block}")
                    else:
                        if(pos.is_overlap(other_pos)):
                            res.append(block)
                            rospy.loginfo(f"XX add {block}")
                            while(not initial.is_clear_block(block)):
                                block = initial.get_block_above(block)
                                res.append(block)
                                rospy.loginfo(f"YY add {block}")
        return res

    def search_overlap_with_goal_of_other_block(self, initial, goal):
        """Searches for a block whose current position overlaps with the goal position of another block.

        This function detects mispositioned blocks by identifying those whose position
        overlaps with the goal position of another block.

        @param initial: The initial state of the blocks.
        @type initial: State
        @param goal: The target (goal) state of the blocks.
        @type goal: State
        @return: The mispositioned block overlapping the goal position of another block, or None if no overlap is found.
        @rtype: Block or None
        """
        for block in initial.blocks:
            for other_block in goal.blocks:
                if block.id != other_block.id:  # Avoid self-comparison
                    if block.position.is_overlap(other_block.position):
                        if block.position.get_z() == other_block.position.get_z():
                            rospy.loginfo(f"Block {block.id} overlaps the goal of Block {other_block.id}")
                            return block
        return None

    def search_wrong_bottom(self, initial, goal):
        """Case where b1 is on b2 and b2 is a bad bottom block"""
        try:
            for b1 in initial.blocks:
                if(b1.get_z() > 0):
                    b1_goal = goal.get_block_from_id(b1.id)
                    if(b1.get_z() != b1_goal.get_z()):
                        #case where b1's level is wrong
                        to_fix = b1
                        while(not initial.is_clear_block(to_fix)):
                            to_fix = initial.get_block_above(to_fix)
                        return to_fix
                    elif(b1.position != b1_goal.position):
                        #case where b1's position is wrong (wrong stack)
                        to_fix = b1
                        while(not initial.is_clear_block(to_fix)):
                            to_fix = initial.get_block_above(to_fix)
                        return to_fix
                    else:
                        initial_bottom = initial.get_block_below(b1)
                        goal_bottom = initial.get_block_below(b1)
                        if(goal_bottom.id != initial_bottom.id):
                            #case where bottom is wrong
                            to_fix = b1
                            while(not initial.is_clear_block(to_fix)):
                                to_fix = initial.get_block_above(to_fix)
                            return to_fix
        except StateException:
            raise StateException

    def solve(self):
        """Solve the block positioning problem.

        This method compares the initial and goal states and creates a plan to fix any
        blocks that are not correctly placed. It creates pick-and-place tasks for
        blocks that need to be moved.

        @return: A plan containing tasks to correct block placements.
        @rtype: Plan
        """

        plan = Plan()

        try:
            initial = State(BlockFactory.create(self.block_poses))
            goal = State(BlockFactory.create(self.goal))

            # to_fix = self.search_in_both_initial_goal(initial, goal)
            # if(to_fix):
            #     plan.task.append(self.fix_both_error(to_fix, goal))
            #     return plan
            #
            # to_fix = self.search_overlap_with_goal_of_other_block(initial, goal)
            # if(to_fix):
            #     plan.task.append(self.fix_overlap_other_goal_error(to_fix))
            #     return plan

            to_fix = self.search_overlap_with_goal(initial, goal)
            for b_to_fix in to_fix[::-1]:
                print(b_to_fix)
                plan.task.append(self.fix_overlap_other_goal_error(b_to_fix))
            return plan
        except StateException:
            print("BUGGGGG")
            return plan

    def ad_hoc_planning_service(self, req):
        """ROS service handler for ad hoc planning.

        This method serves as the handler for the ad hoc planning service.
        It triggers the solve method to generate a plan for fixing block positions.

        @param req: The service request, containing problem data.
        @type req: AdHocPlanCmdRequest
        @return: A plan with tasks to correct block placements.
        @rtype: Plan
        """
        rospy.loginfo("New probem received")
        return self.solve()

    def fix_both_error(self, block, goal):
        """Create a Pick-and-Place task to fix block position errors.

        This method generates a Pick-and-Place task to move a block from its current
        incorrect position to the correct position specified in the goal.

        @param block: The block to be corrected.
        @type block: Block
        @param goal: The goal state containing the correct block positions.
        @type goal: State
        @return: A PickNPlace task to move the block to the correct position.
        @rtype: PickNPlace
        """
        pick = Pick()
        b1 = Block()
        b1.id = block.id
        b1.top_left = block.position.top_left
        b1.top_right = block.position.top_right
        b1.bottom_left = block.position.bottom_left
        b1.bottom_right = block.position.bottom_right
        pick.block = b1
        place = Place()
        to_place = goal.get_block_from_id(block.id)
        b2 = Block()
        b2.id = to_place.id
        b2.top_left = to_place.position.top_left
        b2.top_right = to_place.position.top_right
        b2.bottom_left = to_place.position.bottom_left
        b2.bottom_right = to_place.position.bottom_right
        place.block = b2
        pick_n_place = PickNPlace()
        pick_n_place.pick = pick
        pick_n_place.place = place
        pick_n_place.agent = "operator"
        pick_n_place.action_id = 0
        return pick_n_place

    def fix_overlap_other_goal_error(self, block):
        """Creates a Pick-and-Place task to move a block that overlaps with another block's goal position.

        This function generates a task to move a block from its current incorrect overlapping position
        to the correct position specified in the goal.

        @param block: The block to be repositioned.
        @return: A PickNPlace task to move the block to the correct position.
        @rtype: PickNPlace
        """
        # Define the Pick action for the block
        pick = Pick()
        b1 = Block()
        b1.id = block.id
        b1.color = block.color
        b1.top_left = block.position.top_left
        b1.top_right = block.position.top_right
        b1.bottom_left = block.position.bottom_left
        b1.bottom_right = block.position.bottom_right
        pick.block = b1

        # Define the Place action for the block in the goal position
        place = Place()
        b2 = Block()
        b2.id = block.id
        b2.color = block.color
        b2.top_left = Point(-1.0,-1.0,-1.0)
        b2.top_right = Point(-1.0,-1.0,-1.0)
        b2.bottom_left = Point(-1.0,-1.0,-1.0)
        b2.bottom_right = Point(-1.0,-1.0,-1.0)
        place.block = b2

        # Combine Pick and Place into a PickNPlace action
        pick_n_place = PickNPlace()
        pick_n_place.pick = pick
        pick_n_place.place = place
        pick_n_place.agent = "operator"
        pick_n_place.action_id = 0  # Assign an action ID if needed

        return pick_n_place


    def fix_inversed_error(self, block, goal):
        print("Fix inverse")
        pick = Pick()
        b1 = Block()
        b1.id = block.id
        b1.color = block.color
        b1.top_left = block.position.top_left
        b1.top_right = block.position.top_right
        b1.bottom_left = block.position.bottom_left
        b1.bottom_right = block.position.bottom_right
        pick.block = b1
        place = Place()
        to_place = goal.get_block_from_id(block.id)
        b2 = Block()
        b2.id = b1.id
        b2.color = b1.color
        b2.top_left.x = -1
        b2.top_left.y = -1
        b2.top_left.z = -1
        b2.top_right = b2.top_left
        b2.bottom_left = b2.top_left
        b2.bottom_right = b2.top_left
        place.block = b2
        pick_n_place = PickNPlace()
        pick_n_place.pick = pick
        pick_n_place.place = place
        pick_n_place.agent = "operator"
        pick_n_place.action_id = 0
        return pick_n_place

    def fix_wrong_bottom_error(self, block, goal):
        print("Fix wrong bottom")
        pick = Pick()
        b1 = Block()
        b1.id = block.id
        b1.color = block.color
        b1.top_left = block.position.top_left
        b1.top_right = block.position.top_right
        b1.bottom_left = block.position.bottom_left
        b1.bottom_right = block.position.bottom_right
        pick.block = b1
        place = Place()
        to_place = goal.get_block_from_id(block.id)
        b2 = Block()
        b2.id = b1.id
        b2.color = b1.color
        b2.top_left.x = -1
        b2.top_left.y = -1
        b2.top_left.z = -1
        b2.top_right = b2.top_left
        b2.bottom_left = b2.top_left
        b2.bottom_right = b2.top_left
        place.block = b2
        pick_n_place = PickNPlace()
        pick_n_place.pick = pick
        pick_n_place.place = place
        pick_n_place.agent = "operator"
        pick_n_place.action_id = 0
        return pick_n_place

def main():
    rospy.init_node("ad_hoc_planner", anonymous=True)
    Planner()
    rospy.spin()


if __name__ == "__main__":
    main()
