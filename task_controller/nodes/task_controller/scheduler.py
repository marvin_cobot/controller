#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" A task scheduler to share tasks between agents."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import rospy
import json
import os, rospkg, yaml

from state_controller.msg import Block, BlockState
from task_controller.msg import Pick, Place, PickNPlace, Plan

STATES_CONFIG_FILE = "config/states.yaml"
AGENTS_CONFIG_FILE = "config/agents.yaml"

def get_block_from_id(block_state, id):
    """Retrieve a block from the block state using its unique identifier."""
    for b in block_state.blocks:
        if b.id == id:
            return b
    return None

class Scheduler:
    """The Task Scheduler.
    """

    def __init__(self):
        """Constructs a Task Scheduler."""
        self.block_poses = BlockState()
        self.goal = BlockState()
        self.load_config()
        self.plan = Plan()

        rospy.Subscriber(
            self.states_topics['block_poses'],
            BlockState,
            self.read_block_poses)

        rospy.Subscriber(
            self.states_topics['goal'],
            BlockState,
            self.read_goal)

        rospy.Subscriber(
            self.states_topics['plan'],
            Plan,
            self.read_plan)

        self.agent_pub = {}
        self.agent_actions = {}
        self.agent_topics = {}

        for agent in self.agents.keys():
            self.agent_actions[agent] = None
            self.agent_topics[agent] = f"/controller/task_controller/{agent}_action"
            self.agent_pub[agent] = rospy.Publisher(\
                self.agent_topics[agent],\
                Plan,\
                queue_size=0)

    def load_config(self):
        """Load configuration files for states and agents."""
        rospack = rospkg.RosPack()
        path = rospack.get_path("task_controller")

        rospy.loginfo("Read States Config")
        with open(path + "/" + STATES_CONFIG_FILE, 'r') as file:
            self.states_topics = yaml.safe_load(file)

        rospy.loginfo("Agents Config")
        with open(path + "/" + AGENTS_CONFIG_FILE, 'r') as file:
            self.agents = yaml.safe_load(file)

    def read_block_poses(self, block_poses):
        """Read and update the current block poses."""
        self.block_poses = block_poses

    def read_goal(self, goal):
        """Read and update the goal block poses."""
        self.goal = goal

    def read_plan(self, plan):
        """Read and update the current plan."""
        self.plan = plan
        self.upd_next_actions()

    def is_feasible_action(self, pick_n_place):
        """Check if the action is feasible based on the current block poses."""
        return pick_n_place.feasible  # Assuming PickNPlace has a feasible attribute

    def get_next_feasible_operator_action(self, agent):
        """Get the next feasible action for the specified agent."""
        for pick_n_place in self.plan.task:
            if pick_n_place.agent == agent and not pick_n_place.done:
                if self.is_feasible_action(pick_n_place):
                    return pick_n_place
        return None

    def upd_next_actions(self):
        """Update the next actions for each agent."""
        for agent in self.agents.keys():
            new_plan_agent = Plan()
            next_act = self.get_next_feasible_operator_action(agent)
            if next_act:
                self.agent_actions[agent] = next_act
                new_plan_agent.task.append(next_act)

            self.agent_pub[agent].publish(new_plan_agent)

def main():
    rospy.init_node("scheduler", anonymous=True)
    Scheduler()
    rospy.spin()

if __name__ == "__main__":
    main()
