#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" A plan checker."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import rospy
import json
import os, rospkg, yaml

from task_controller.msg import Plan, PickNPlace
from task_controller.srv import CheckPlanCmd
from state_controller.msg import Block, BlockState
from task_controller.logical_state import State, BlockFactory, Position, StateException

STATES_CONFIG_FILE = "config/states.yaml"
AGENTS_CONFIG_FILE = "config/agents.yaml"

def get_block_from_id(id, state):
    """
    Retrieves a block with the specified ID from a given state.

    This function iterates through all the blocks in the provided state and returns
    the block that matches the specified ID.

    @param id: The unique identifier of the block to find.
    @type id: int
    @param state: The state that contains the blocks to search.
    @type state: State
    @return: The block with the matching ID, or None if no block is found.
    @rtype: Block or None
    """
    for block in state.blocks:
        if (id == block.id):
            return block

class PlanChecker:
    """The PlanChecker class verifies and checks the execution of a plan.

    This class listens to block pose updates and goal updates and checks
    if the plan needs to be adjusted (re-planning) based on the current state.

    Attributes:
        block_poses (BlockState): Current positions of all blocks.
        goal (BlockState): Desired goal positions of all blocks.
        states_topics (dict): Topics configuration loaded from YAML.
        agents (dict): Agents configuration loaded from YAML.
    """
    def __init__(self):
        """Constructs the PlanChecker and subscribes to relevant topics."""
        self.block_poses = BlockState()
        self.goal = BlockState()

        self.load_config()
        rospy.loginfo(f"Plan Checker subscribes to the {self.states_topics['block_poses']} topic")
        rospy.Subscriber(
            self.states_topics['block_poses'],
            BlockState,
            self.read_block_poses)

        rospy.loginfo(f"Plan Checker subscribes to the {self.states_topics['goal']} topic")
        rospy.Subscriber(
            self.states_topics['goal'],
            BlockState,
            self.read_goal)

        rospy.Service('/controller/task_controller/check_plan', CheckPlanCmd, self.service_plan_checking)

    def load_config(self):
        """Load configuration files for states and agents.

        This method reads YAML configuration files that define the state topics and agents
        required for the Plan checking process.
        """
        rospack = rospkg.RosPack()
        path = rospack.get_path("task_controller")

        rospy.loginfo("Read States Config")
        with open(path + "/" + STATES_CONFIG_FILE, 'r') as file:
            self.states_topics = yaml.safe_load(file)

        rospy.loginfo("Read Agents Config")
        with open(path + "/" + AGENTS_CONFIG_FILE, 'r') as file:
            self.agents = yaml.safe_load(file)

    def read_block_poses(self, block_poses):
        """Read and update the current block poses.

        Updates the internal representation of block poses with the provided data.

        @param block_poses: The new block poses to be set.
        """
        self.block_poses = block_poses

    def read_goal(self, goal):
        """Read and update the goal block poses.

        Updates the internal representation of the goal with the provided data.

        @param goal: The new goal block poses to be set.
        """
        self.goal = goal

    def service_plan_checking(self, req):
        """Service to check if the current plan needs adjustment.

        Processes the incoming plan and verifies whether a re-plan is required
        by comparing the pick and place actions with the current state.

        @param req: The request containing the plan to be checked.
        @type req: CheckPlanCmdRequest
        @return: The modified plan and a flag indicating if a re-plan is needed.
        @rtype: (Plan, bool)
        """
        rospy.loginfo("New plan received")
        act_id = 0
        new_plan = Plan()
        replan = False

        for pick_n_place in req.plan.task:
            pick = pick_n_place.pick
            place = pick_n_place.place
            agent = pick_n_place.agent

            # Check if pick is done
            pick_done = self.pick_is_done(pick)
            # Check if place is done
            place_done = self.place_is_done(place)

            new_pick_n_place = PickNPlace()
            new_pick_n_place.agent = agent
            new_pick_n_place.action_id = act_id
            new_pick_n_place.pick = pick
            new_pick_n_place.place = place


            if pick_done and place_done:
                new_pick_n_place.done = True
                new_pick_n_place.error = False  # No error
            else:
                #Check pick place feasibility
                try:
                    current_state = State(BlockFactory.create(self.block_poses))
                    new_pick_n_place.feasible =\
                        current_state.is_pick_n_place_feasible(
                            pick.block.id,
                            Position(
                                top_left=place.block.top_left,
                                top_right=place.block.top_right,
                                bottom_left=place.block.bottom_left,
                                bottom_right=place.block.bottom_right
                            )
                        )
                except StateException:
                    new_pick_n_place.feasible = False
                if pick_done and not place_done:
                    new_pick_n_place.done = True
                    new_pick_n_place.error = True  # Pick OK, Wrong Place
                    replan = True  # Indicate that replan is needed since the plan is wrong
                else:
                    new_pick_n_place.done = False  # No Pick
                    new_pick_n_place.error = False  # No Error
            new_plan.task.append(new_pick_n_place)
            act_id += 1

        # Find the first action that is not done and check its feasibility
        for task in new_plan.task:
            if not task.done:  # Check if the action is not done
                if not task.feasible:
                    replan = True  # The first non-done action is not feasible
                break  # Exit loop after finding the first non-done action

        return new_plan, replan

    def pick_is_done(self, pick):
        """Check if the pick action has been completed.

        Compares the pick block's current position with its expected position
        in the block_poses to determine if the action is completed.

        @param pick: The pick action to be checked.
        @type pick: Pick
        @return: True if the pick action is done, False otherwise.
        @rtype: bool
        """
        block = get_block_from_id(pick.block.id, self.block_poses)
        return\
            pick.block.top_left.x != block.top_left.x or \
            pick.block.top_left.y != block.top_left.y or \
            pick.block.top_left.z != block.top_left.z or \
            pick.block.top_right.x != block.top_right.x or \
            pick.block.top_right.y != block.top_right.y or \
            pick.block.top_right.z != block.top_right.z or \
            pick.block.bottom_left.x != block.bottom_left.x or \
            pick.block.bottom_left.y != block.bottom_left.y or \
            pick.block.bottom_left.z != block.bottom_left.z or \
            pick.block.bottom_right.x != block.bottom_right.x or \
            pick.block.bottom_right.y != block.bottom_right.y or \
            pick.block.bottom_right.z != block.bottom_right.z

    def place_is_done(self, place):
        """Check if the place action has been completed.

        Compares the place block's current position with its expected position
        in the block_poses to determine if the action is completed.

        @param place: The place action to be checked.
        @type place: Place
        @return: True if the place action is correctly done, False otherwise.
        @rtype: bool
        """
        block = get_block_from_id(place.block.id, self.block_poses)
        return\
            place.block.top_left.x == block.top_left.x and \
            place.block.top_left.y == block.top_left.y and \
            place.block.top_left.z == block.top_left.z and \
            place.block.top_right.x == block.top_right.x and \
            place.block.top_right.y == block.top_right.y and \
            place.block.top_right.z == block.top_right.z and \
            place.block.bottom_left.x == block.bottom_left.x and \
            place.block.bottom_left.y == block.bottom_left.y and \
            place.block.bottom_left.z == block.bottom_left.z and \
            place.block.bottom_right.x == block.bottom_right.x and \
            place.block.bottom_right.y == block.bottom_right.y and \
            place.block.bottom_right.z == block.bottom_right.z

def main():
    rospy.init_node("plan_checker", anonymous=True)
    PlanChecker()
    rospy.spin()

if __name__ == "__main__":
    main()
