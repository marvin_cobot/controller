#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" A pddl interpretor to high level perception and generate planning problem."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import rospy
import json
import os, rospkg, yaml

from state_controller.msg import Block, BlockState
from task_controller.msg import Pick, Place, PickNPlace, Plan
from rosplan.msg import PddlPlan, PddlAction, PddlProblem
from rosplan.srv import SolvePddlCmd, SolvePddlCmdRequest
from task_controller.srv import CheckPlanCmd, CheckPlanCmdRequest
from task_controller.srv import AdHocPlanCmd, AdHocPlanCmdRequest
from std_msgs.msg import String, Bool
from task_controller.logical_state import Problem, ProblemSolvedException, StateException

STATES_CONFIG_FILE = "config/states.yaml"
AGENTS_CONFIG_FILE = "config/agents.yaml"
TOPICS_CONFIG_FILE = "config/topics.yaml"
LIFECYCLE_CONFIG_FILE = "config/lifecycle.yaml"
ZONES_CONFIG_FILE = "config/zones.yaml"

def get_block_from_id(block_state, id):
    """Retrieve a block from the block state using its unique identifier.

    This function searches through the list of blocks in the provided block state
    and returns the block that matches the given identifier.

    Parameters:
        block_state (State): An instance of the State class containing a collection
                             of blocks.
        id (str): The unique identifier of the block to be retrieved.

    Returns:
        Block or None: The block that matches the given id, or None if no block
                       with that id is found.
    """
    for b in block_state.blocks:
        if b.id == id:
            return b
    return None

class Interpretor:
    """The PDDL Interpreter.

    This class is responsible for interpreting PDDL (Planning Domain Definition Language)
    tasks and generating plans to achieve specified goals based on the current state of blocks.

    It manages the configuration of block poses and goals, communicates with ROS (Robot Operating System) services,
    and constructs a plan for execution.

    Attributes:
        block_poses (BlockState): The current state of block poses.
        goal (BlockState): The desired goal state for the blocks.
        states_topics (dict): Configuration data for state topics.
        agents (dict): Configuration data for agents.
        pddl_plan (PddlPlan): The PDDL plan generated from the problem definition.
        plan (Plan): The final execution plan created from the PDDL plan.
    """

    def __init__(self):
        """Constructs a PDDL Interpreter.

        Initializes the interpreter by loading configuration files for states and agents,
        and preparing data structures for block poses, goals, and plans.
        """
        self.block_poses = BlockState()
        self.goal = BlockState()
        self.load_config()
        self.pddl_plan = PddlPlan()
        self.plan = Plan()
        self.initial_block_position = {}
        self.authority = ''
        self.started = False

        #init subscribers
        rospy.loginfo(f"Pddl Interpretors subscribes to the {self.states_topics['block_poses']} topic")
        rospy.Subscriber(
            self.states_topics['block_poses'],
            BlockState,
            self.read_block_poses)

        rospy.loginfo(f"Pddl Interpretors subscribes to the {self.states_topics['goal']} topic")
        rospy.Subscriber(
            self.states_topics['goal'],
            BlockState,
            self.read_goal)

        rospy.loginfo(f"Pddl Interpretors subscribes to the {self.lifecycle_topics['authority']} topic")
        rospy.Subscriber(
            self.lifecycle_topics['authority'],
            String,
            self.authority_callback)

        rospy.loginfo(f"Pddl Interpretors subscribes to the {self.lifecycle_topics['started']} topic")
        rospy.Subscriber(
            self.lifecycle_topics['started'],
            Bool,
            self.started_callback)

        #init publisher
        self.pub_plan = rospy.Publisher(
            self.pub_topics['plan'],
            Plan,
            queue_size=0)
        rospy.loginfo(f"{self.pub_topics['plan']} ready")

        #reset plan
        self.reset_plan()

    def load_config(self):
        """Load configuration files for states and agents.

        This method reads YAML configuration files that define the state topics and agents
        required for the PDDL planning process.
        """
        rospack = rospkg.RosPack()
        path = rospack.get_path("task_controller")

        rospy.loginfo("Read States Config")
        with open(path + "/" + STATES_CONFIG_FILE, 'r') as file:
            self.states_topics = yaml.safe_load(file)

        rospy.loginfo("Agents Config")
        with open(path + "/" + AGENTS_CONFIG_FILE, 'r') as file:
            self.agents = yaml.safe_load(file)

        rospy.loginfo('Topics publisher Config')
        with open(path + "/" + TOPICS_CONFIG_FILE, 'r') as file:
            self.pub_topics = yaml.safe_load(file)

        rospy.loginfo("Lifecycle Config")
        self.lifecycle_topics = {}
        with open(path + "/" + LIFECYCLE_CONFIG_FILE, 'r') as file:
            lifecycle_config = yaml.safe_load(file)
            self.lifecycle_topics['authority'] = lifecycle_config.get("authority", "/controller/authority")
            self.lifecycle_topics['started'] = lifecycle_config.get("started", "/started")

        rospy.loginfo("Zones Config")
        with open(path + "/" + ZONES_CONFIG_FILE, 'r') as file:
            self.zones = yaml.safe_load(file)

    def publish(self):
        """Publish the plan"""
        self.pub_plan.publish(self.plan)

    def read_block_poses(self, block_poses):
        """Read and update the current block poses.

        Updates the internal representation of block poses with the provided data.

        @param block_poses: The new block poses to be set.
        """
        self.block_poses = block_poses

        #Save position as initial pose each block that is not in the assembly zone
        for block in self.block_poses.blocks:
            if(not self.is_in_assembly(block)):
                self.initial_block_position[block.id] = block

        if(len(self.goal.blocks) > 0):
            self.upd_plan()

    def read_goal(self, goal):
        """Read and update the goal block poses.

        Updates the internal representation of the goal with the provided data.

        @param goal: The new goal block poses to be set.
        """
        self.goal = goal

    def reset_plan(self):
        """Reset the plan to an empty state."""
        self.plan = Plan()

    def authority_callback(self, msg):
        """Callback to process data from the authority topic."""
        self.authority = msg.data

    def started_callback(self, msg):
        """Callback to process data from the started topic."""
        self.started = msg.data

    def is_empty_plan(self):
        """Check if the plan is empty

        @return True if the plan is empty, false otherwise
        @rtype Bool
        """
        return len(self.plan.task) <= 0

    def is_on_goal(self, block):
        """Check if the block is on its goal position.

        @param block: The block to check

        @return True if block is on goal position, False otherwise
        @rtype Bool
        """
        goal_block = get_block_from_id(self.goal, block.id)
        if(not goal_block):
            return False
        return\
            block.top_left.x == goal_block.top_left.x and\
            block.top_left.y == goal_block.top_left.y and\
            block.top_left.z == goal_block.top_left.z and\
            block.bottom_left.x == goal_block.bottom_left.x and\
            block.bottom_left.y == goal_block.bottom_left.y and\
            block.bottom_left.z == goal_block.bottom_left.z and\
            block.top_right.x == goal_block.top_right.x and\
            block.top_right.y == goal_block.top_right.y and\
            block.top_right.z == goal_block.top_right.z and\
            block.bottom_right.x == goal_block.bottom_right.x and\
            block.bottom_right.y == goal_block.bottom_right.y and\
            block.bottom_right.z == goal_block.bottom_right.z

    def is_problem_solved(self):
        """Check if all blocks are in their goal positions.

        @return True if the problem is solved.
        @rtype Bool
        """
        for block in self.block_poses.blocks:
            if(not self.is_on_goal(block)):
                return False
        return True

    def get_position_to(self, id_block, str_pos):
        """Get the position from the str_pos returned by the planner"""
        tab = str_pos.split('_')
        id = int(str_pos.split('_')[len(tab)-3])
        if(str_pos.split('_')[len(tab)-1] == 'to'):
            return get_block_from_id(self.goal, id)
        elif(str_pos.split('_')[len(tab)-1] == 'from'):
            return get_block_from_id(self.block_poses, id)
        else:
            return self.initial_block_position[id_block]

    def is_in_zone(self, block, zone):
        """Check if the block is within the specified zone boundaries.

        @param block: The block to check.
        @param zone: The zone boundaries with 'top_left', 'top_right', 'bottom_left', 'bottom_right'.

        @return True if the block is within the zone, False otherwise.
        @rtype Bool
        """
        return (
            zone["top_left"]["x"] <= block.top_left.x <= zone["top_right"]["x"] and
            zone["top_left"]["y"] <= block.top_left.y <= zone["bottom_left"]["y"] and
            zone["top_left"]["x"] <= block.top_right.x <= zone["top_right"]["x"] and
            zone["top_left"]["y"] <= block.top_right.y <= zone["bottom_left"]["y"] and
            zone["top_left"]["x"] <= block.bottom_left.x <= zone["top_right"]["x"] and
            zone["top_left"]["y"] <= block.bottom_left.y <= zone["bottom_left"]["y"] and
            zone["top_left"]["x"] <= block.bottom_right.x <= zone["top_right"]["x"] and
            zone["top_left"]["y"] <= block.bottom_right.y <= zone["bottom_left"]["y"]
        )

    def is_in_assembly(self, block):
        """Check if the block is within the assembly zone.

        @param block: The block to check.

        @return True if the block is within the assembly zone, False otherwise.
        @rtype Bool
        """
        assembly_zone = self.zones.get("assembly")
        if not assembly_zone:
            rospy.logwarn("Assembly zone configuration not found.")
            return False
        return self.is_in_zone(block, assembly_zone)

    def check_plan(self):
        """Check the current plan for feasibility."""
        req = CheckPlanCmdRequest()
        req.plan = self.plan
        proxy = rospy.ServiceProxy('/controller/task_controller/check_plan', CheckPlanCmd)
        checkplan_resp = proxy(req)
        return [checkplan_resp.plan, checkplan_resp.replan]

    def is_in_plan(self, pick_place):
        for other in self.plan.task:
            if(other.pick.block.id == pick_place.pick.block.id):
                return\
                    other.pick.block.top_left == pick_place.pick.block.top_left and\
                    other.pick.block.top_right == pick_place.pick.block.top_right and\
                    other.pick.block.bottom_right == pick_place.pick.block.bottom_right and\
                    other.pick.block.bottom_left == pick_place.pick.block.bottom_left and\
                    other.place.block.top_left == pick_place.place.block.top_left and\
                    other.place.block.top_right == pick_place.place.block.top_right and\
                    other.place.block.bottom_right == pick_place.place.block.bottom_right and\
                    other.place.block.bottom_left == pick_place.place.block.bottom_left

    def call_adhoc_planner(self):
        """Call the ad-hoc planner."""
        req = AdHocPlanCmdRequest()
        proxy = rospy.ServiceProxy(\
            '/controller/task_controller/ad_hoc_planner',\
            AdHocPlanCmd)
        resp = proxy(req)
        fix_plan = Plan()
        for pick_place in resp.plan.task:
            place = pick_place.place
            if(place.block.top_left.x == -1):
                place_initial = Place()
                place_initial.block = self.initial_block_position[place.block.id]
                pick_place.place = place_initial
            if(not self.is_in_plan(pick_place)):
                fix_plan.task.append(pick_place)
        for pick_place in self.plan.task:
            fix_plan.task.append(pick_place)
        self.plan = fix_plan

    def call_pddl_planner(self):
        """Call the PDDL planner to generate a new plan."""
        problem = Problem(self.block_poses, self.goal, self.agents)
        rospack = rospkg.RosPack()
        path = rospack.get_path("task_controller")
        instance_file = path+"/database/instance.pddl"
        rospy.loginfo("Generate "+instance_file)
        problem.generate_pddl(instance_file)

        domain_file = path+"/database/domain.pddl"
        req = SolvePddlCmdRequest()
        req.problem.domain = domain_file
        req.problem.instance = instance_file

        proxy = rospy.ServiceProxy('/rosplan/solve_pddl', SolvePddlCmd)

        pddl_plan = proxy(req)
        return pddl_plan

    def translate_pddl_plan(self, pddl_plan):
        """Translate the received PDDL plan into a ROS Plan.

        @param pddl_plan: the PDDL plan
        """
        self.plan = Plan()
        act_id = 0
        for a in pddl_plan.plan.actions:
            agent = a.pddl_action.split(" ")[1]
            tab = a.pddl_action.split(" ")[2].split("_")
            id = int(tab[len(tab)-1])
            to_pick = get_block_from_id(self.block_poses, id)
            pick = Pick()
            pick.block = to_pick
            place = Place()
            place.block = self.get_position_to(to_pick.id, a.pddl_action.split(" ")[4])
            pick_place = PickNPlace()
            pick_place.action_id = act_id
            pick_place.agent = agent
            pick_place.pick = pick
            pick_place.place = place

            self.plan.task.append(pick_place)
            act_id += 1

    def solve_problem(self):
        """Generate a new plan based on the current problem state."""
        try:
            pddl_plan = self.call_pddl_planner()

            if(pddl_plan.sucess):
                self.translate_pddl_plan(pddl_plan)
            rospy.loginfo("Call Ad Hoc planner to fix errors")
            self.call_adhoc_planner()
        except StateException:
            rospy.loginfo("Impossible to solve the problem")
            rospy.loginfo("Call Ad Hoc planner to fix errors")
            self.call_adhoc_planner()

    def upd_plan(self):
        """Update the execution plan based on the current block poses and goals.

        This method generates a new PDDL problem instance, sends it to a ROS service for
        solving, and constructs the execution plan based on the received actions.

        """
        #Case where assembly is not started : wait and reset plan
        if(not self.started):
            self.reset_plan()

        #Case where assembly is started and plan is still empty: solve the problem
        if(self.started and self.is_empty_plan()):
            try:
                self.solve_problem()
                [self.plan, _] = self.check_plan()
            except ProblemSolvedException:
                #Probleùm is already solved, no planning needed
                rospy.loginfo("Problem already solved")
            except StateException:
                #Probleùm is already solved, no planning needed
                rospy.loginfo("Problem with state detection\nWaiting for another state")

        #Check the non empty plan
        replan = True
        [self.plan, replan] = self.check_plan()

        #Case where replan is required and authority is cobotic
        #Note: Even if replan is required, other authority than cobotic disallow replanning
        if(replan and self.authority == 'cobotic'):
            try:
                self.solve_problem()
                [self.plan, _] = self.check_plan()
            except ProblemSolvedException:
                #Probleùm is already solved, no planning needed
                rospy.loginfo("Problem already solved")
            except StateException:
                #Probleùm is already solved, no planning needed
                rospy.loginfo("Problem with state detection\nWaiting for another state")

        self.publish()

def main():
    rospy.init_node("pddl_interpretor", anonymous=True)
    Interpretor()
    rospy.spin()


if __name__ == "__main__":
    main()
