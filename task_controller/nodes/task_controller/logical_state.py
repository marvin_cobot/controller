#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" A logical interpretor of high level perception."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import queue
from abc import ABC, abstractmethod
from geometry_msgs.msg import Point
import rospy

class ProblemSolvedException(Exception):
    """Custom exception raised when a problem is solved."""
    pass

class InversedStackException(Exception):
    """Custom exception raised when a stack is inversed
    for instance: b1 on b2 instead of b2 on b1."""
    pass

class StateException(Exception):
    """Custom exception raised when a state
    is impossible."""
    pass

def equals(point1, point2):
    """
    Compare two 3D points to check if their x, y, and z coordinates are equal.

    @param point1: The first point to compare. Must have attributes x, y, and z.
    @param point2: The second point to compare. Must have attributes x, y, and z.

    @return: True if point1 and point2 have the same x, y, and z values, False otherwise.
    @rtype: bool
    """
    return point1.x == point2.x and\
        point1.y == point2.y and\
        point1.z == point2.z

def str_point(point):
    """
    Convert a 3D point to a string representation in the format 'x_y_z'.

    @param point: The point to convert. Must have attributes x, y, and z.

    @return: A string representing the point in the format 'x_y_z', where x, y, and z
             are the respective coordinates of the point.
    @rtype: str
    """
    return f"{point.x}_{point.y}_{point.z}"

def is_a_cube(pose):
    """
    Checks if the given set of points corresponds to the corners of a cube.

    A cube is defined as a block where the horizontal and vertical distances between
    `top_left` and `bottom_right` are each 1 unit, forming a 2x2 square.

    @param pose: A `Position` object containing four points: `top_left`, `top_right`,
                 `bottom_left`, and `bottom_right`. Each point has `x` and `y` coordinates.
    @returns: True if the points form a cube, otherwise False.
    @rtype: bool
    """
    return pose.top_left.x+1 == pose.bottom_right.x and\
        pose.top_left.y+1 == pose.bottom_right.y

def is_a_brick(pose):
    """
    Checks if the given set of points corresponds to the corners of a brick.

    A brick is a rectangular block where one dimension is longer than the other.
    It can be in two possible configurations:
    - 4x2 (horizontal brick)
    - 2x4 (vertical brick)

    @param pose: A `Position` object containing four points: `top_left`, `top_right`,
                 `bottom_left`, and `bottom_right`. Each point has `x` and `y` coordinates.
    @returns: True if the points form a brick, otherwise False.
    @rtype: bool
    """
    return\
        (pose.top_left.x+3 == pose.bottom_right.x and\
            pose.top_left.y+1 == pose.bottom_right.y )or\
        (pose.top_left.x+1 == pose.bottom_right.x and\
            pose.top_left.y+3 == pose.bottom_right.y)


class Position:
    """
    Represents a 3D position defined by four corner points:
    `top_left`, `top_right`, `bottom_left`, and `bottom_right`.

    Each corner point should be an object with `x`, `y`, and `z` coordinates.
    This class provides methods to check if a point is inside the position,
    if two positions overlap, or if one position is on top of another.
    """
    def __init__(self, top_left, top_right, bottom_left, bottom_right):
        """
        Initializes a `Position` object with four corner points.

        @param top_left: The top-left corner point of the position.
        @param top_right: The top-right corner point of the position.
        @param bottom_left: The bottom-left corner point of the position.
        @param bottom_right: The bottom-right corner point of the position.
        """
        self.top_left = top_left
        self.top_right = top_right
        self.bottom_left = bottom_left
        self.bottom_right = bottom_right

    def get_z(self):
        """
        Returns the z-coordinate of the position.

        @returns: The z-coordinate of the `top_left` corner of the position.
        @rtype: int or float
        """
        return self.top_left.z

    def is_inside(self, point):
        """
        Checks if a given point is inside the bounds of the position.

        A point is considered inside the position if its `x` and `y` coordinates
        are within the boundaries defined by the `top_left` and `bottom_right` corners.

        @param point: A point object with `x` and `y` coordinates.
        @returns: True if the point is inside the position, False otherwise.
        @rtype: bool
        """
        return self.top_left.x <= point.x and point.x <= self.bottom_right.x and\
            self.top_left.y <= point.y and point.y <= self.bottom_right.y

    def is_overlap(self, position_other):
        """
        Checks if the current position overlaps with another position.

        Overlap occurs if any of the corners of `position_other` fall within
        the bounds of the current position.

        @param position_other: Another `Position` object to check for overlap.
        @returns: True if the positions overlap, False otherwise.
        @rtype: bool
        """
        return self.is_inside(position_other.top_left)\
            or self.is_inside(position_other.top_right)\
            or self.is_inside(position_other.bottom_left)\
            or self.is_inside(position_other.bottom_right)

    def is_on(self, position_other):
        """
        Checks if the current position is directly on top of another position.

        The current position is considered to be "on" the other position if:
        - The positions overlap (determined by `is_overlap` method).
        - The current position is exactly 1 unit higher in the z-axis than the other.

        @param position_other: Another `Position` object to check for relative height.
        @returns: True if the current position is on top of the other, False otherwise.
        @rtype: bool
        """
        return self.is_overlap(position_other) and\
            self.top_left.z == position_other.top_left.z+1

    def __eq__(self, other):
        """
        Checks if two Position objects are equal by comparing their corner points.

        @param other: The object to compare with the current Position instance.
                      It is expected to be another Position object.

        @return: True if all four corner points (top_left, bottom_left, bottom_right,
                 top_right) of both Position objects are equal, otherwise False.
        @rtype: bool
        """
        if(isinstance(other, Position)):
            return\
                equals(self.top_left, other.top_left) and\
                equals(self.bottom_left, other.bottom_left) and\
                equals(self.bottom_right, other.bottom_right) and\
                equals(self.top_right, other.top_right)
        return False

    def __ne__(self, other):
        """
        Checks if two Position objects are not equal.

        @param other: The object to compare with the current Position instance.
                      It is expected to be another Position object.

        @return: True if the two Position objects are not equal, otherwise False.
        @rtype: bool
        """
        return not self.__eq__(other)


    def str(self):
        """
        Returns a string representation of the Position object.

        The string contains the coordinates of the four corner points (top_left,
        top_right, bottom_right, bottom_left) in the format:
        "top_left_top_right_bottom_right_bottom_left".

        @return: A string representing the Position object.
        @rtype: str
        """
        return f"position:\n{self.top_left}\n{self.top_right}\n{self.bottom_right}\n{self.bottom_left}"

    def __str__(self):
        """
        Returns the string representation of the Position object.

        This method calls the `str()` method and returns the result.

        @return: A string representing the Position object.
        @rtype: str
        """
        return self.str()

class Block(ABC):
    """
    Abstract representation of a Block in a 3D space.

    Each block has a position, defined by four corner points (`top_left`, `top_right`,
    `bottom_left`, `bottom_right`), and a color. Blocks can be checked for their relative
    positions in the 3D space (e.g., if one block is on top of another).

    Attributes:
        position (Position): The position of the block, represented by a `Position` object.
        color (str): The color of the block.
    """

    def __init__(self, position, color, id):
        """
        Initializes a Block with a given position and color.

        @param position: A `Position` object that defines the block's location in 3D space.
        @param color: A string representing the color of the block.
        @param id: The block ID
        """
        self.position = position
        self.color = color
        self.id = id

    def is_on(self, block_other):
        """
        Checks if the current block is directly on top of another block.

        This method calls `is_on` from the `Position` class to determine if the current block
        is stacked on the `block_other`.

        @param block_other: Another `Block` object to check relative position.
        @returns: True if the current block is on top of the other block, otherwise False.
        @rtype: bool
        """
        return self.position.is_on(block_other.position)

    def get_z(self):
        """
        Returns the z-coordinate of the block.

        @returns: The z-coordinate of the `top_left` corner of the block.
        @rtype: int or float
        """
        return self.position.top_left.z

    def __str__(self):
        """
        Returns a string representation of the block.

        This method calls the `str` method, which must be implemented by any subclass.

        @returns: A string describing the block.
        """
        return self.str()

    @abstractmethod
    def str(self):
        """
        Abstract method to return a string representation of the block.

        Subclasses must implement this method to provide a string that describes
        the specific type of block (e.g., a red block, blue block).

        @returns: A string representation of the block.
        """
        return f"{self.color}_block"

    def __hash__(self):
        """
        Generates a hash value for the Block object.

        The hash is computed using the string representation of the Block,
        which is obtained via the `str()` method. This allows Block objects
        to be used in hash-based collections like sets and as dictionary keys.

        @return: The hash value of the Block object.
        @rtype: int
        """
        return hash(self.str())


class Cube(Block):
    """
    Represents a Cube, which is a type of Block.

    A Cube has a specific shape (a 2x2 square in the x and y axes) and a color.
    It inherits from the `Block` class and implements the abstract `str` method
    to return a description of the cube.

    Attributes:
        position (Position): The position of the cube in 3D space.
        color (str): The color of the cube.
    """

    def __init__(self, position, color, id):
        """
        Initializes a Cube with a given position and color.

        @param position: A `Position` object that defines the cube's location in 3D space.
        @param color: A string representing the color of the cube.
        @param id: The block ID.
        """
        super().__init__(position, color, id)

    def str(self):
        """
        Returns a string representation of the cube.

        This implementation of the abstract method from the `Block` class describes
        the cube with its color.

        @returns: A string in the format "<color>_cube_<id>", e.g., "red_cube_0".
        """
        return f"{self.color}_cube_{self.id}"

    def __hash__(self):
        """
        Generates a hash value for the Cube object.

        The hash is computed using the string representation of the Cube,
        which is obtained via the `str()` method. This allows Cube objects
        to be used in hash-based collections like sets and as dictionary keys.

        @return: The hash value of the Cube object.
        @rtype: int
        """
        return hash(self.str())

class Brick(Block):
    """
    Represents a Brick, which is a type of Block.

    A Brick has a specific shape (a rectangular form, either 4x2 or 2x4 in the x and y axes)
    and a color. It inherits from the `Block` class and implements the abstract `str` method
    to return a description of the brick.

    Attributes:
        position (Position): The position of the brick in 3D space.
        color (str): The color of the brick.
    """
    def __init__(self, position, color, id):
        """
        Initializes a Brick with a given position and color.

        @param position: A `Position` object that defines the brick's location in 3D space.
        @param color: A string representing the color of the brick.
        @param id: The block ID.
        """
        super().__init__(position, color, id)

    def str(self):
        """
        Returns a string representation of the brick.

        This implementation of the abstract method from the `Block` class describes
        the brick with its color.

        @returns: A string in the format "<color>_brick_<id>", e.g., "blue_brick_0".
        """
        return f"{self.color}_brick_{self.id}"

    def __hash__(self):
        """
        Generates a hash value for the Brick object.

        The hash is computed using the string representation of the Brick,
        which is obtained via the `str()` method. This allows Brick objects
        to be used in hash-based collections like sets and as dictionary keys.

        @return: The hash value of the Brick object.
        @rtype: int
        """
        return hash(self.str())


class BlockFactory:
    """
    A factory class responsible for creating blocks from a given block state message.

    This factory method processes a message that contains information about blocks
    and returns a list of `Block` objects (either `Cube` or `Brick`) based on the
    shape of the block defined by its position.

    Method:
        create(blockstate_msg): Converts block state data into `Cube` or `Brick` objects.
    """

    @staticmethod
    def create(blockstate_msg):
        """
        Creates a list of blocks (either `Cube` or `Brick`) from a block state message.

        For each block in `blockstate_msg`, this method creates a `Position` object
        and determines whether the block is a `Cube` or a `Brick` using the helper
        functions `is_a_cube`. It then instantiates the appropriate block type and
        appends it to the list of blocks.

        @param blockstate_msg: A message object containing block state information.
                               It is expected to have a `blocks` attribute, which is
                               an iterable of block data, where each block has
                               attributes `top_left`, `top_right`, `bottom_left`,
                               `bottom_right`, and `color`.
        @returns: A list of `Block` objects, either `Cube` or `Brick`, depending on
                  the shape of each block.
        @rtype: list[Block]
        """
        blocks = []
        for b in blockstate_msg.blocks:
            # Create a Position object from the block data
            pose = Position(b.top_left, b.top_right, b.bottom_left, b.bottom_right)

            # Determine if it's a Cube or a Brick and create the appropriate block
            blocks.append(Cube(pose, b.color, b.id) if is_a_cube(pose) else Brick(pose, b.color, b.id))
        return blocks

class State:
    """
    Represents the state of a set of blocks in a 3D space.

    The State class maintains the arrangement of blocks, including their stacking
    relationships. It provides methods to query the arrangement of blocks, such
    as determining which blocks are on the table, which blocks are clear, and
    the relationships between blocks.

    Attributes:
        blocks (list[Block]): A list of Block objects representing the blocks in the state.
        stacks (dict): A dictionary mapping blocks to their stacks.
                       Each block maps to a dictionary of blocks directly on top of it.
    """
    def __init__(
        self,
        blocks,
        assembly={"x":{"min": 14, 'max':30}, "y":{"min": 2, 'max':14}}):
        """
        Initializes the state with a given list of blocks and establishes their stacking relationships.

        @param blocks: A list of Block objects that represent the blocks in the state.
        @param assembly: Assembly zone boundaries.
        """
        self.blocks = blocks
        self.stacks = {}

        # Initialize stacks for blocks at z=0 (on the table)
        for b in self.blocks:
            if(b.get_z() == 0):
                self.stacks[b] = {}

        self.assembly = assembly
        # Establish stacking relationships for all blocks
        z = 1
        try:
            while(z <=  self.get_max_z()):
                for b in self.blocks:
                    if(b.get_z() == z):
                        for b1 in self.blocks:
                            if(b.is_on(b1)):
                                self.add_on_transition(b, b1)
                z += 1
        except StateException:
            raise StateException


    def is_in_assembly_point(self, p):
        """
        Check if a pointis within the assembly zone.

        @param point: A Point object to check.
        @return True if the block is within the assembly zone, False otherwise.
        @rtype bool: True if the block is within the assembly zone, False otherwise.
        """
        return \
            self.assembly["x"]["min"] <= p.x <= self.assembly["x"]["max"] and \
            self.assembly["y"]["min"] <= p.y <= self.assembly["y"]["max"]

    def is_in_assembly(self, block):
        """
        Check if a block is within the assembly zone based on its corner positions.

        @param block: A Block object to check.
        @return bool: True if the block is within the assembly zone, False otherwise.
        """
        return \
            self.is_in_assembly_point(block.position.top_left) or\
            self.is_in_assembly_point(block.position.top_right) or\
            self.is_in_assembly_point(block.position.bottom_left) or\
            self.is_in_assembly_point(block.position.bottom_right)

    def get_blocks_in_assembly(self):
        """
        Returns a list of blocks that are within the assembly zone.

        @return list[Block]: A list of Block objects that are within the assembly boundaries.
        """
        blocks_in_assembly = []

        for block in self.blocks:
            if self.is_in_assembly(block):
                blocks_in_assembly.append(block)
        return blocks_in_assembly

    def contains(self, block):
        """
        Checks if a given block is present in the current state.

        This method iterates over all blocks in the current state and checks
        if there is a block with the same ID, color, and position as the given block.

        @param block: The block to check.
        @type block: Block
        @return: True if the block is present, otherwise False.
        @rtype: bool
        """
        for b in self.blocks:
            if (b.id == block.id and b.color == block.color and\
                b.position == block.position):
                return True
        return False

    def containsAll(self, state):
        """
        Checks if all blocks from another state are present in the current state.

        This method iterates over the blocks in the given state and verifies if
        each block is contained in the current state.

        @param state: The state whose blocks need to be checked.
        @type state: State
        @return: True if all blocks from the given state are present, otherwise False.
        @rtype: bool
        """
        for b in state.blocks:
            if (not self.contains(b)):
                return False
        return True

    def is_position_used(self, position):
        """
        Checks if a given position is already used by any block in the current state.

        @param position: The position to check.
        @type position: Position
        @return: True if the position is used by any block, otherwise False.
        @rtype: bool
        """
        for block in self.blocks:
            if(block.position == position):
                return True
        return False

    def get_block_from_position_used(self, position):
        """
        Checks if a given position is already used by any block in the current state.

        @param position: The position to check.
        @type position: Position
        @return: Block on the used position.
        @rtype: Block or None
        """
        for block in self.blocks:
            if(block.position == position):
                return block
        return False


    def is_position_overlaped(self, position):
        """
        Checks if a given position overlaps with the position of any block in the current state.

        @param position: The position to check for overlap.
        @type position: Position
        @return: True if the position overlaps with any block's position, otherwise False.
        @rtype: bool
        """
        for block in self.blocks:
            if(block.position.is_overlap(position)):
                return True
        return False

    def is_position_overlaped_same_z(self, position):
        """
        Checks if a given position overlaps with the position of any block in the current state.

        @param position: The position to check for overlap.
        @type position: Position
        @return: True if the position overlaps with any block's position, otherwise False.
        @rtype: bool
        """
        for block in self.blocks:
            if(block.position.get_z() == position.get_z()):
                if(block.position.is_overlap(position)):
                    return True
        return False


    def get_block_from_overlaped_position(self, position):
        """
        Retrieves the block that overlaps with a given position, if any.

        @param position: The position to check for overlap.
        @type position: Position
        @return: The block that overlaps with the given position, or None if no block overlaps.
        @rtype: Block or None
        """
        for block in self.blocks:
            if(block.position.is_overlap(position)):
                return block
        return None

    def get_block_from_position(self, position):
        """
        Retrieves the block that occupies a given position, if any.

        @param position: The position to check for a block.
        @type position: Position
        @return: The block at the given position, or None if no block is found at the position.
        @rtype: Block or None
        """
        for block in self.blocks:
            if(block.position == position):
                return block
        return None

    def get_block_from_id(self, id):
        """Retrieve a block by its ID.

        This method searches through the blocks in the current state and returns the block
        that matches the specified ID. If no block with the given ID is found, it returns None.

        @param id: The unique identifier of the block to be retrieved.
        @type id: int
        @return: The block object that matches the given ID, or None if not found.
        @rtype: Block or None
        """
        for b in self.blocks:
            if(b.id == id):
                return b
        return None

    def get_max_z(self):
        """
        Gets the maximum z-coordinate among all blocks.

        @returns: The maximum z-coordinate found in the blocks.
        @rtype: int
        """
        z_max = 0
        for b in self.blocks:
            if(b.get_z() > z_max):
                z_max = b.get_z()
        return z_max

    def get_ontable_blocks(self):
        """
        Retrieves a list of blocks that are on the table (z = 0).

        @returns: A list of blocks that are on the table.
        @rtype: list[Block]
        """
        return list(self.stacks.keys())

    def get_not_ontable_blocks(self):
        """
        Retrieves a list of blocks that are not on the table (z > 0).

        @returns: A list of blocks that are on the table.
        @rtype: list[Block]
        """
        res = []
        for block in self.blocks:
            if(block.get_z() > 0):
                res.append(block)
        return res

    def get_clear_blocks(self, current_stacks):
        """
        Retrieves a list of blocks that have no blocks on top of them.

        @param current_stacks: The current stacks of blocks to check.
        @returns: A list of blocks that are clear (no blocks on top).
        @rtype: list[Block]
        """
        res = []
        for b in current_stacks.keys():
            if(len(current_stacks[b].keys()) <= 0):
                res.append(b)
            else:
                for b1 in self.get_clear_blocks(current_stacks[b]):
                    res.append(b1)
        return res

    def is_clear_block(self, block):
        clear_blocks = self.get_clear_blocks(self.stacks)
        for b in clear_blocks:
            if(b.id == block.id):
                return True
        return False

    def get_on_blocks(self, current_stacks):
        """
        Retrieves a list of pairs of blocks where one block is on top of another.

        @param current_stacks: The current stacks of blocks to check.
        @returns: A list of pairs of blocks (top block, bottom block).
        @rtype: list[list[Block]]
        """
        res = []
        for b in current_stacks.keys():
            if(len(current_stacks[b].keys()) <= 0):
                continue
            else:
                for b1 in current_stacks[b].keys():
                    res.append([b1, b])
                    for [top,bottom] in self.get_on_blocks(current_stacks[b]):
                        res.append([top,bottom])
        return res

    def get_block_above(self, bottom):
        """Get the block above the bottom block"""
        for b in self.blocks:
            if(b.is_on(bottom)):
                return b

    def get_block_below(self, top):
        """Get the block above the bottom block"""
        for b in self.blocks:
            if(top.is_on(b)):
                return b


    def __str__(self):
        """
        Returns a string representation of the state of blocks.

        The string representation includes information about blocks that are on the table,
        blocks that are clear, and the stacking relationships between blocks.

        @returns: A string representation of the state.
        @rtype: str
        """
        return self.str()

    def str(self):
        """
        Generates a formatted string that describes the state of blocks.

        @returns: A string describing the relationships and states of the blocks.
        @rtype: str
        """
        res = ""
        for b in self.get_ontable_blocks():
            res += f"(ontable {b})\n"
        for [top,bottom] in self.get_on_blocks(self.stacks):
            res += f"(on {top} {bottom})\n"
        for b in self.get_clear_blocks(self.stacks):
            res += f"(clear {b})\n"

        return res

    def is_on_table(self, b):
        """
        Checks if a block is on the table (z-coordinate is 0).

        @param b: A Block object to check.
        @returns: True if the block is on the table, otherwise False.
        @rtype: bool
        """
        return b.get_z() == 0

    def get_predecessors(self, current_stacks, b):
        """
        Retrieves the predecessors of a block (blocks that are directly below it).

        @param current_stacks: The current stacks of blocks to check.
        @param b: A Block object to find predecessors for.
        @returns: A queue of predecessor blocks or None if there are no predecessors.
        @rtype: queue.LifoQueue or None
        """
        if(len(current_stacks.keys()) <= 0):
            return None
        else:
            if(b in current_stacks.keys()):
                return queue.LifoQueue()
            else:
                for b1 in current_stacks.keys():
                    q = self.get_predecessors(current_stacks[b1], b)
                    if(q == None):
                        continue
                    else:
                        q.put(b1)
                        return q

    def add_on_transition(self, top, bottom):
        """
        Adds a transition indicating that one block is on top of another.

        This method determines if the bottom block is on the table or if it has
        predecessors, and updates the stacks accordingly.

        @param top: The Block that is on top.
        @param bottom: The Block that is below.
        """
        if(self.is_on_table(bottom)):
            self.stacks[bottom][top] = {}
        else:
            current_stacks = self.stacks
            pred = self.get_predecessors(current_stacks, bottom)
            if(not pred):
                raise StateException
            while not pred.empty():
                b = pred.get()
                current_stacks = current_stacks[b]
            current_stacks[bottom][top] = {}

    def is_pick_n_place_feasible(self, id, new_pos):
        """
        Check if it is possible to pick up the block with the given `id` (i.e., it is clear)
        and place it at the new position (`new_pos`). The new position must:
          - Not overlap with other blocks in the current state.
          - If it has a z > 0, the position directly below (at z-1) must be used or overlapped.


        @param id: The unique identifier of the block to pick and place.
        @type id: int
        @param new_pos: The new position where the block is intended to be placed.
        @type new_pos: Position

        @return: True if the block with the given `id` is clear and `new_pos` is free, otherwise False.
        @rtype: bool
        """
        #Retrieve the block by ID
        block = self.get_block_from_id(id)

        #Check if the block is clear

        if (not self.is_clear_block(block)):
            return False

        #Check if the new position is free (not used or overlapped)
        if (self.is_position_used(new_pos)):
            return False
        if (self.is_position_overlaped_same_z(new_pos)):
            #Case where ID overlaped the goal destination
            id_overlaped = self.get_block_from_overlaped_position(new_pos).id
            if(id != id_overlaped):
                return False

        #Ensure that if z > 0, the position directly below (z-1) is used or overlapped
        if new_pos.top_left.z > 0:
            point_below_tl = Point(new_pos.top_left.x, new_pos.top_left.y, new_pos.top_left.z - 1)
            point_below_tr = Point(new_pos.top_right.x, new_pos.top_right.y, new_pos.top_right.z - 1)
            point_below_bl = Point(new_pos.bottom_left.x, new_pos.bottom_left.y, new_pos.bottom_left.z - 1)
            point_below_br = Point(new_pos.bottom_right.x, new_pos.bottom_right.y, new_pos.bottom_right.z - 1)

            #Create a new Position for the position below
            pos_below = Position(
                top_left=point_below_tl,
                top_right=point_below_tr,
                bottom_left=point_below_bl,
                bottom_right=point_below_br
            )

            # Check if the position below is used or overlapped
            if (not (self.is_position_used(pos_below) or self.is_position_overlaped_same_z(pos_below))):
                return False  # Return False if pos_below is neither used nor overlapped
        return True

class Problem:
    """
    Represents an assembly problem involving initial and goal block states, and agents.

    The Problem class encapsulates the initial state of blocks, the desired goal state,
    and the agents involved in the assembly process. It provides methods to generate
    a PDDL representation of the problem instance.

    Attributes:
        initial (State): The initial state of blocks in the assembly problem.
        goal (State): The goal state of blocks in the assembly problem.
        agents (dict): A dictionary mapping agent names to their types.
    """
    def __init__(self, init_blockstate, goal_blockstate, agents):
        """
        Initializes the problem with initial and goal block states, and agents.

        @param init_blockstate: The initial block state, used to create the initial State.
        @param goal_blockstate: The goal block state, used to create the goal State.
        @param agents: A dictionary of agents involved in the assembly.
        """
        self.initial = State(BlockFactory.create(init_blockstate))
        self.goal = State(BlockFactory.create(goal_blockstate))
        if(self.initial.containsAll(self.goal)):
            raise ProblemSolvedException
        self.agents = agents

        self.positions = {}
        for block in self.initial.blocks:
            self.positions[block.str()] = {'origin':f"{block}_position_from"}
        for block in self.goal.blocks:
            self.positions[block.str()]['destin'] =f"{block}_position_to"
            if(self.initial.is_position_used(block.position)):
                block_in_init = self.initial.get_block_from_position(block.position)
                if(block_in_init.id != block.id):
                    self.positions[block.str()]['destin'] =\
                        self.positions[block_in_init.str()]['origin']


    def generate_pddl(self, instance_file):
        """
        Generates a PDDL instance file for the assembly problem.

        The generated PDDL includes the problem definition, domain, objects,
        initial state, and goal state.

        @param instance_file: The file path where the PDDL instance will be saved.
        """

        str = ""
        str = f"(define (problem assembly_instance)"
        str = f"{str}\n\t(:domain assembly)\n"
        str = f"{str}{self.generate_objects()}\n"
        str = f"{str}{self.generate_initial()}\n"
        str = f"{str}{self.generate_goal()}"
        str = f"{str}\n)"
        with open(instance_file, 'w') as file:
            file.write(str)

    def generate_objects(self):
        """
        Generates the object declarations for the PDDL instance.

        This method generates declarations for agents, blocks, and block positions
        in the format required by PDDL.

        @returns: A string containing the PDDL object declarations.
        @rtype: str
        """
        str = f"\n\t(:objects "

        # Generate agents
        for agent in self.agents.keys():
            str = f"{str}\n\t\t{agent} - {self.agents[agent]['type']}"

        # Generate blocks
        str = f"{str}\n\t\t"
        for block in self.initial.blocks:
            str = f"{str}{block} "
        str = f"{str}- block"

        # Generate block positions
        all_positions = []
        str = f"{str}\n\t\t"
        for block in self.initial.blocks:
            if(self.goal.contains(block)):
                #Case where b is at the correct position in the initial state
                continue
            if(not self.positions[block.str()]['origin'] in all_positions):
                str = f"{str}{self.positions[block.str()]['origin']} "
                all_positions.append(self.positions[block.str()]['origin'])
        str = f"{str}- position"
        str = f"{str}\n\t\t"
        for block in self.goal.blocks:
            if(not self.positions[block.str()]['destin'] in all_positions):
                str = f"{str}{self.positions[block.str()]['destin']} "
                all_positions.append(self.positions[block.str()]['destin'])
        str = f"{str}- position"
        if(len(self.initial.get_blocks_in_assembly()) >= 0):
            str = f"{str}\n\t\t"
            for block in self.initial.get_blocks_in_assembly():
                str = f"{str}{block}_position_initial "
            str = f"{str}- position"

        str = f"{str}\n\t)"

        return str

    def generate_initial(self):
        """
        Generates the initial state declarations for the PDDL instance.

        This method generates facts about the initial positions of blocks, their
        stacking relationships, and which blocks are clear.

        @returns: A string containing the PDDL initial state declarations.
        @rtype: str
        """
        str = f"\n\t(:init "
        #Block position facts
        for block in self.initial.blocks:
            if(self.goal.contains(block)):
                #Case where b is at the correct position in the initial state
                str = f"{str}\n\t\t(at {block} {self.positions[block.str()]['destin']})"
            else:
                str = f"{str}\n\t\t(at {block} {self.positions[block.str()]['origin']} )"

        #Position stack facts
        str = f"{str}\n\n\t\t"
        for [top, bottom] in self.initial.get_on_blocks(self.initial.stacks):
            str = f"{str}(on {self.positions[top.str()]['origin']}  {self.positions[bottom.str()]['origin']}) "
        str = f"{str}\n\t\t"
        for [top, bottom] in self.goal.get_on_blocks(self.goal.stacks):
            str = f"{str}(on {self.positions[top.str()]['destin']}  {self.positions[bottom.str()]['destin']}) "

        #Cell position facts
        # str = f"{str}\n\n\t\t"
        # for block in self.initial.get_clear_blocks(self.initial.stacks):
        #     str = f"{str}(clear {block}_position_from) "
        str = f"{str}\n\t\t"
        for block in self.goal.get_ontable_blocks():
            if(self.initial.is_position_used(block.position)):
                #Case where b is at the correct position in the initial state
                rospy.loginfo(f"used {block} goal position by {self.initial.get_block_from_position_used(block.position)}")
                continue
            if(self.initial.is_position_overlaped(block.position)):
                #Case where b is at the correct position in the initial state
                if(self.initial.get_block_from_overlaped_position(
                    block.position).id != block.id):
                    rospy.loginfo(f"overlaped {block} goal position by {self.initial.get_block_from_overlaped_position(block.position)}")
                    rospy.loginfo(block.position)
                    rospy.loginfo(self.initial.get_block_from_overlaped_position(block.position).position)
                    continue
            str = f"{str}(clear {self.positions[block.str()]['destin']}) "

        str = f"{str}\n\t\t"
        for block in self.initial.get_clear_blocks(self.initial.stacks):
            if(self.goal.contains(block)):
                #case where block is in the goal position
                str = f"{str}(clear-above {self.positions[block.str()]['destin']}) "
                block2 = self.goal.get_block_above(block)
                if(block2):
                    #If there exist b2 such that b2 on b in the goal state,
                    #then the b2's position in goal is clear
                    str = f"{str}(clear {self.positions[block2.str()]['destin']}) "
            else:
                str = f"{str}(clear-above {self.positions[block.str()]['origin']}) "

        str = f"{str}\n\t\t"
        for block in self.initial.get_blocks_in_assembly():
            str = f"{str}(clear {block}_position_initial)"
            str = f"{str}(allowed {block} {block}_position_initial)"

        str = f"{str}\n\t\t"
        for block in self.goal.blocks:
            str = f"{str}(allowed {block} {block}_position_to)"

        #Block costs
        for block in self.initial.blocks:
            for agent in self.agents.keys():
                if(self.agents[agent]['position'] == 'south'):
                    cost = int(24 - block.position.top_left.y)
                else:
                    cost = int(block.position.top_left.y)
                str = f"{str}\n\t\t(= (agent-cost {agent} {block}) {cost})"
        str = f"{str}\n\t\t(= (total-cost) 0)"
        str = f"{str}\n\t)"
        return str

    def generate_goal(self):
        """
        Generates the goal state declarations for the PDDL instance.

        This method generates the goal conditions that specify the desired
        state of blocks in the assembly problem.

        @returns: A string containing the PDDL goal state declarations.
        @rtype: str
        """
        str = f"\n\t(:goal (and"
        for block in self.goal.blocks:
            str = f"{str}\n\t\t(at {block} {self.positions[block.str()]['destin']})"
        str = f"{str}\n\t))"
        str = f"{str}\n\t(:metric minimize (total-cost)\n\t)"
        return str
