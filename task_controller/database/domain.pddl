;-----------------------
;   Lego domain (3D)   |
;-----------------------
(define
    (domain assembly)
    ;-------------------
    ;   Requirements   |
    ;-------------------
    (:requirements
        :strips
        :typing
        ;:negative-preconditions
        :conditional-effects
        :action-costs
        :universal-preconditions
    )
    ;----------------
    ;   Types       |
    ;----------------
    (:types
        agent block position - object
        cube brick - block
        human robot - agent
    )
    ;----------------
    ;   Functions   |
    ;----------------
    (:functions
      (total-cost)
      (agent-cost ?a - agent ?b - block)
    )
    ;-------------------------------------------------------------------------
    ;                          Predicates                                    |
    ;-------------------------------------------------------------------------
    (:predicates
        ; position predicates
        (clear ?p - position)
        (clear-above ?p - position)
        (on ?p1 ?p2 - position)

        ; block predicates
        (at ?b - block ?p - position)
        (destin ?b - block ?p - position)
        (allowed ?b - block ?p - position)

    )

    ;------------------------------------------------------------------------
    ;                          Actions                                      |
    ;------------------------------------------------------------------------
    ;;
    (:action pick_place
    :parameters (
            ?a - agent
            ?b - block
            ?p_origin ?p_destin - position)
    :precondition
        (and
            (at ?b ?p_origin)
            (clear ?p_destin)
            ;(not (on ?p_destin ?p_origin))
            (clear-above ?p_origin)
            (allowed ?b ?p_destin)
        )
    :effect
        (and

            (not(at ?b ?p_origin))
            (at ?b ?p_destin)

            (not (clear ?p_destin))
            (clear ?p_origin)

            (not (clear-above ?p_origin))
            (clear-above ?p_destin)

            ; Change clear values
            (forall (?p - position)
                (when
                    ;Antecedent
                    (on ?p ?p_destin)
                    ;Consequence
                    (and (clear ?p))
                )
            )

            ; Change clear values
            (forall (?p - position)
                (when
                    ;Antecedent
                    (on ?p ?p_origin)
                    ;Consequence
                    (and (not(clear ?p)))
                )
            )

            ; Change clear values
            (forall (?p - position)
                (when
                    ;Antecedent
                    (on ?p_origin ?p)
                    ;Consequence
                    (and (clear-above ?p))
                )
            )

            ; Change clear values
            (forall (?p - position)
                (when
                    ;Antecedent
                    (on ?p_destin ?p)
                    ;Consequence
                    (and (not(clear-above ?p)))
                )
            )

            (increase (total-cost) (agent-cost ?a ?b))
        )
    )
)
