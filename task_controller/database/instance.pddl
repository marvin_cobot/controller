(define (problem assembly_instance)
	(:domain assembly)

	(:objects 
		operator - human
		white_cube_0 orange_cube_1 olive_cube_2 yellow_cube_3 red_cube_4 blue_cube_5 purple_cube_6 pink_cube_7 light_blue_cube_8 light_yellow_cube_9 - block
		white_cube_0_position_from orange_cube_1_position_from olive_cube_2_position_from yellow_cube_3_position_from red_cube_4_position_from blue_cube_5_position_from purple_cube_6_position_from pink_cube_7_position_from light_blue_cube_8_position_from light_yellow_cube_9_position_from - position
		white_cube_0_position_to orange_cube_1_position_to olive_cube_2_position_to yellow_cube_3_position_to red_cube_4_position_to blue_cube_5_position_to purple_cube_6_position_to pink_cube_7_position_to light_blue_cube_8_position_to light_yellow_cube_9_position_to - position
		- position
	)

	(:init 
		(at white_cube_0 white_cube_0_position_from )
		(at orange_cube_1 orange_cube_1_position_from )
		(at olive_cube_2 olive_cube_2_position_from )
		(at yellow_cube_3 yellow_cube_3_position_from )
		(at red_cube_4 red_cube_4_position_from )
		(at blue_cube_5 blue_cube_5_position_from )
		(at purple_cube_6 purple_cube_6_position_from )
		(at pink_cube_7 pink_cube_7_position_from )
		(at light_blue_cube_8 light_blue_cube_8_position_from )
		(at light_yellow_cube_9 light_yellow_cube_9_position_from )

		
		
		(clear white_cube_0_position_to) (clear orange_cube_1_position_to) (clear olive_cube_2_position_to) (clear yellow_cube_3_position_to) (clear red_cube_4_position_to) (clear blue_cube_5_position_to) (clear purple_cube_6_position_to) (clear pink_cube_7_position_to) (clear light_blue_cube_8_position_to) (clear light_yellow_cube_9_position_to) 
		(clear-above white_cube_0_position_from) (clear-above orange_cube_1_position_from) (clear-above olive_cube_2_position_from) (clear-above yellow_cube_3_position_from) (clear-above red_cube_4_position_from) (clear-above blue_cube_5_position_from) (clear-above purple_cube_6_position_from) (clear-above pink_cube_7_position_from) (clear-above light_blue_cube_8_position_from) (clear-above light_yellow_cube_9_position_from) 
		
		(allowed white_cube_0 white_cube_0_position_to)(allowed orange_cube_1 orange_cube_1_position_to)(allowed olive_cube_2 olive_cube_2_position_to)(allowed yellow_cube_3 yellow_cube_3_position_to)(allowed red_cube_4 red_cube_4_position_to)(allowed blue_cube_5 blue_cube_5_position_to)(allowed purple_cube_6 purple_cube_6_position_to)(allowed pink_cube_7 pink_cube_7_position_to)(allowed light_blue_cube_8 light_blue_cube_8_position_to)(allowed light_yellow_cube_9 light_yellow_cube_9_position_to)
		(= (agent-cost operator white_cube_0) 12)
		(= (agent-cost operator orange_cube_1) 20)
		(= (agent-cost operator olive_cube_2) 16)
		(= (agent-cost operator yellow_cube_3) 16)
		(= (agent-cost operator red_cube_4) 20)
		(= (agent-cost operator blue_cube_5) 12)
		(= (agent-cost operator purple_cube_6) 12)
		(= (agent-cost operator pink_cube_7) 16)
		(= (agent-cost operator light_blue_cube_8) 12)
		(= (agent-cost operator light_yellow_cube_9) 16)
		(= (total-cost) 0)
	)

	(:goal (and
		(at white_cube_0 white_cube_0_position_to)
		(at orange_cube_1 orange_cube_1_position_to)
		(at olive_cube_2 olive_cube_2_position_to)
		(at yellow_cube_3 yellow_cube_3_position_to)
		(at red_cube_4 red_cube_4_position_to)
		(at blue_cube_5 blue_cube_5_position_to)
		(at purple_cube_6 purple_cube_6_position_to)
		(at pink_cube_7 pink_cube_7_position_to)
		(at light_blue_cube_8 light_blue_cube_8_position_to)
		(at light_yellow_cube_9 light_yellow_cube_9_position_to)
	))
	(:metric minimize (total-cost)
	)
)