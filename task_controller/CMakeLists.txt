cmake_minimum_required(VERSION 3.0.2)
project(task_controller)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  rosplan
  rospy
  message_generation
  state_controller
  std_msgs
)

catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## Generate messages in the 'msg' folder
add_message_files(
  FILES
  Pick.msg
  Place.msg
  PickNPlace.msg
  Plan.msg
  Errors.msg
)

## Generate services in the 'srv' folder
add_service_files(
  FILES
  CheckPlanCmd.srv
  AdHocPlanCmd.srv
)

## Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  std_msgs
  rosplan
  state_controller
)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if your package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  CATKIN_DEPENDS rosplan rospy state_controller std_msgs message_runtime
)

catkin_install_python(PROGRAMS
  nodes/${PROJECT_NAME}/pddl_interpretor.py
  nodes/${PROJECT_NAME}/scheduler.py
  nodes/${PROJECT_NAME}/logical_state.py
  nodes/${PROJECT_NAME}/plan_checker.py
  nodes/${PROJECT_NAME}/ad_hoc_planner.py
  nodes/${PROJECT_NAME}/errors_handler.py
  nodes/${PROJECT_NAME}/end_checker.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

#############
## Install ##
#############

install(
  DIRECTORY database config
  DESTINATION "${CATKIN_PACKAGE_SHARE_DESTINATION}"
)
